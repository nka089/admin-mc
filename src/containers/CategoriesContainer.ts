import CategoriesView, { CategoriesWidgetProperties } from "../widgets/views/categoriesView/CategoriesView";
import Store from "@dojo/framework/stores/Store";
import { createCategory, deleteCategory } from "../processes/CategoriesProcesses";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";

function getProperties(store: Store<State>): CategoriesWidgetProperties {
    const { get, path } = store;
    return {
        categories: get(path("categories")) || [],
        createCategory: (name: string) => createCategory(store, name),
        deleteCategory: (id: string) => deleteCategory(store, id)
    };
}

export default StoreContainer(CategoriesView, "state", {
    paths: path => [path("categories")],
    getProperties
});
