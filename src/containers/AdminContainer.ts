import { DNode } from "@dojo/framework/core/interfaces";
import { v, w } from "@dojo/framework/core/vdom";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Store from "@dojo/framework/stores/Store";
import HTTPRequest from "src/restservice/HTTPRequest";
import { ServicePaths } from "src/restservice/ServicePaths";
import { State } from "src/typings/store";
import Analytics from "src/widgets/analytics/Analytics";
import StoreContainer from "./StoreContainer";

interface AdminViewProperties {
    deletItemById: (id: string) => void;
}

class AdminView extends WidgetBase<AdminViewProperties> {
    // private id = "";
    protected render(): DNode {
        return v("div", [
            // v("div", { styles: { display: "flex", padding: "50px 10px" } }, [
            //     v("p", ["Delete item by ID "]),
            //     v("div", { styles: { width: "200px" } }, [
            //         w(Input, {
            //             onInput: (value: string) => {
            //                 this.id = value;
            //             }
            //         })
            //     ]),
            //     w(
            //         Button,
            //         {
            //             onClick: () => {
            //                 this.properties.deletItemById(this.id);
            //             }
            //         },
            //         ["Entfernen"]
            //     )
            // ]),
            w(Analytics, {})
        ]);
    }
}

function getProperties(store: Store<State>): AdminViewProperties {
    const props: AdminViewProperties = <AdminViewProperties>{};
    props.deletItemById = (id: string) => {
        if (id) {
            try {
                const request = new HTTPRequest();
                request.deleteRequest(ServicePaths.ITEM_BY_ID(id));
            } catch (e) {
                console.warn(e);
            }
        }
    };
    return props;
}

const AdminContainer = StoreContainer(AdminView, "state", { getProperties });

export default AdminContainer;
