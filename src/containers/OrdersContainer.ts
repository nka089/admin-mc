import { Store } from "@dojo/framework/stores/Store";
import { to } from "src/main";
import { ORDER } from "../processes/OrderProcesses";
import { CONTROL } from "../processes/Processes";
import { State } from "../typings/store";
import OrdersGridView, { OrdersGridViewProperties } from "../widgets/views/ordersGridView/OrdersGridView";
import StoreContainer from "./StoreContainer";

function getProperties(store: Store<State>): OrdersGridViewProperties {
    return {
        getOrdersByNameOrDate: (name: string) => getOrdersNynameOrDate(store, name),
        getLastOrders: (nmb: string) => getLastOrders(store, nmb),
        getOrderById: (id: string) => getOrderById(store, id)
    };
}

const getOrderById = async (store: Store<State>, id: string): Promise<void> => {
    CONTROL.setShowHourlassProcess(store)(true);
    const orders = await ORDER.getOrderById(store, id);
    ORDER.setCurrentordersProcess(store)(orders);

    CONTROL.setShowHourlassProcess(store)(false);
    if (orders && orders[0]) {
        ORDER.setSelectedOrderProcess(store)(orders[0]);
        to("auftrag-bearbeiten", orders[0].id);
    }
};

const getOrdersNynameOrDate = async (store: Store<State>, name: string): Promise<void> => {
    CONTROL.setShowHourlassProcess(store)(true);
    const orders = await ORDER.getOrdersByNameOrdate(store, name);
    ORDER.setCurrentordersProcess(store)(orders);
    CONTROL.setShowHourlassProcess(store)(false);
};

const getLastOrders = async (store: Store<State>, nmb: string): Promise<void> => {
    CONTROL.setShowHourlassProcess(store)(true);
    const orders = await ORDER.getLastOrdders(nmb);
    ORDER.setCurrentordersProcess(store)(orders);
    CONTROL.setShowHourlassProcess(store)(false);
};

export default StoreContainer(OrdersGridView, "state", {
    getProperties
});
