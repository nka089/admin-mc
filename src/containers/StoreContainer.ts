import { createStoreContainer } from "@dojo/framework/stores/StoreInjector";
import { State } from "../typings/store";

export default createStoreContainer<State>();
