import { Store } from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";
import OrdersGrid, { OrdersGridproperties } from "../widgets/ordersGrid/OrdersGrid";
import { Order } from "../typings/types";
import { _store, to } from "../main";
import { ORDER } from "../processes/OrderProcesses";

function getProperties(store: Store<State>): OrdersGridproperties {
    const { get, path } = store;
    return {
        columnConfig,
        orders: get(path("currentOrders"))
    };
}

export const editFunction = (order: Order): void => {
    const s = _store();
    ORDER.setSelectedOrderProcess(s)(order);
    to("auftrag-bearbeiten", order.id);
};

const columnConfig = [
    {
        id: "id",
        title: "ID",
        sortable: true
    },
    {
        id: "customer",
        title: "Name",
        childKeys: ["customer", "lastName"]
    },
    {
        id: "from",
        title: "Von",
        childKeys: ["from", "address"]
    },
    {
        id: "from",
        title: "HVZ",
        flag: true,
        childKeys: ["from", "parkingSlot"]
    },
    {
        id: "to",
        title: "Nach",
        childKeys: ["to", "address"]
    },
    {
        id: "to",
        title: "HVZ",
        flag: true,
        childKeys: ["to", "parkingSlot"]
    },
    {
        id: "distance",
        title: "Strecke, km"
    },
    {
        id: "creationTime",
        sortable: true,
        title: "Erstellung"
    },
    // {
    //     id: "lupd",
    //     sortable: true,
    //     title: "Aktualisierung"
    // },
    {
        id: "date",
        title: "Datum",
        sortable: true
    },
    {
        id: "date_from",
        title: "Von",
        sortable: true
    },
    {
        id: "date_to",
        title: "Bis",
        sortable: true
    },
    {
        id: "workersNumber",
        title: "Mann"
    },
    {
        id: "timeBased",
        title: "St.",
        childKeys: ["timeBased", "hours"]
    },
    {
        id: "transporterNumber",
        title: "3.5"
    },
    {
        id: "t75",
        title: "7.5"
    },

    {
        id: "volume",
        title: "m³"
    },
    {
        id: "sum",
        title: "Preis"
    },
    // {
    //     id: "discount",
    //     title: "Rabatt"
    // },
    {
        id: "id",
        title: "Edit",
        edit: { function: editFunction, text: "Bearbeiten" }
    }
    // {
    //     id: "archived",
    //     title: "Archiv",
    //     flag: true,
    //     filterable: true
    // }
];

export default StoreContainer(OrdersGrid, "state", {
    paths: path => [path("currentOrders")],
    getProperties
});
