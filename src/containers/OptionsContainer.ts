import { Store } from "@dojo/framework/stores/Store";
import HTTPRequest from "../restservice/HTTPRequest";
import { ServicePaths } from "../restservice/ServicePaths";
import { State } from "../typings/store";
import OptionsView, { OptionsViewProperties } from "../widgets/views/optionsView/OptionsView";
import StoreContainer from "./StoreContainer";
import { Option } from "src/typings/types";

const restService = new HTTPRequest();

function getProperties(store: Store<State>): OptionsViewProperties {
    const { get, path } = store;
    return {
        options: get(path("options")) || {},
        updateOption: (key: string, value: string) => updateOption(key, value)
    };
}

const updateOption = async (key: string, value: string): Promise<void> => {
    const body = <Option>{ value };
    try {
        const response = await restService.putRequest(ServicePaths.OPTIONS(key), body);
        alert(response.message);
    } catch (error) {
        alert("something went wrong, try to check the console output");
    }
};

export default StoreContainer(OptionsView, "state", {
    // paths: [["currentData"]],
    getProperties
});
