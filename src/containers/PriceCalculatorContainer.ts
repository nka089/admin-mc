import { Store } from "@dojo/framework/stores/Store";
import { State } from "src/typings/store";
import StoreContainer from "src/containers/StoreContainer";
import PriceCalculator, { PriceCalculatorProperties } from "src/widgets/editor/priceCalculator/PriceCalculator";
import { MasterData } from "src/processes/MasterDataProceses";
import { OPTIONS } from "src/constants/Constants";

function getProperties(store: Store<State>): PriceCalculatorProperties {
    const { get, path } = store;
    const order = get(path("order"));
    const hvzPrice = parseFloat(MasterData.getOption(OPTIONS.HVZ_PRICE));
    // const options = get(path("options", "length"));
    return {
        // options,
        order,
        sum: get(path("order", "sum")),
        hvzPrice
    };
}

export default StoreContainer(PriceCalculator, "state", {
    paths: path => [path("order"), path("control", "updateOrder"), path("order", "sum"), path("options", "length")],
    getProperties
});
