import { Store } from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";
import { ORDER, OrderProcesses } from "src/processes/OrderProcesses";
import OrderEditorWidget, { OrderEditorProperties } from "src/widgets/editor/OrderEditorWidget";
import { Order, Item } from "src/typings/types";

import { DataPayload } from "src/typings/payloads";
import { Calculations } from "src/calculations/Calculations";

import { Utils } from "src/utils/Utils";
import { generatePDF } from "src/pdf/PdfHelper";
import { CONTROL } from "src/processes/Processes";

function getProperties(store: Store<State>): OrderEditorProperties {
    const { get, path } = store;
    return {
        allItems: getFlattenItems(store),
        allServices: get(path("services")),
        setData: (key: string, data: string) => {
            setData(key, data, store);
        },
        setDisposalText: (text: string) => setDisposalText(text, store),
        setText: (text: string) => setText(text, store),
        order: get(path("order")),
        calculate: (order: Order) => calculate(store, order),
        restore: (order: Order) => restore(store, order),
        update: (order: Order) => update(store, order),
        delete: () => deleteOrder(store),
        createPDF,
        showDialog: get(path("control", "showDialog")),
        setShowDialog: (b: boolean) => CONTROL.setShowDialog(store)(b)
    };
}

const getFlattenItems = (store: Store<State>): Item[] => {
    let items = new Array<Item>();

    const { get, path } = store;

    let allItems = get(path("items"));
    if (allItems) {
        for (let i = 0; i < allItems.length; i++) {
            for (let j = 0; j < allItems[i].categoryRefs.length; j++) {
                let categorizedItem = Utils.copyObject<Item>(allItems[i]);
                categorizedItem.categories = [allItems[i].categoryRefs[j].name];
                items.push(categorizedItem);
            }
        }
    }

    return items;
};

const update = async (store: Store<State>, order: Order) => {
    Calculations.calculateSum(order).then(order => {
        ORDER.updateOrder(order, store);
    });
};

const restore = (store: Store<State>, order: Order) => {
    CONTROL.setShowHourlassProcess(store)(true);
    ORDER.getOrderById(store, order.id!).then(updatedOrder => {
        if (updatedOrder[0]) {
            ORDER.replaceOrderProcess(store)(updatedOrder[0]);

            setTimeout(() => {
                CONTROL.setShowHourlassProcess(store)(false);
            }, 500);
        }
    });
};
const calculate = (store: Store<State>, order: Order) => {
    CONTROL.setShowHourlassProcess(store)(true);
    Calculations.calculateSum(order).then(updatedOrder => {
        ORDER.replaceOrderProcess(store)(updatedOrder);
        setTimeout(() => {
            CONTROL.setShowHourlassProcess(store)(false);
        }, 500);
    });
};

const deleteOrder = (store: Store<State>) => {
    ORDER.deleteOrder(store);
};

const createPDF = (order: Order, isTime?: boolean): void => {
    generatePDF(order, isTime);
};

const setDisposalText = (text: string, store: Store<State>): void => {
    OrderProcesses.setDisposalTextProcess(store)(text);
};

const setText = (text: string, store: Store<State>): void => {
    OrderProcesses.setTextProcess(store)(text);
};

const setData = (key: string, data: string, store: Store<State>): void => {
    OrderProcesses.setPersonalDataProcess(store)(<DataPayload>{ key, value: data });
};

export default StoreContainer(OrderEditorWidget, "state", {
    paths: path => [path("order"), path("control", "updateOrder"), path("control", "showDialog")],
    getProperties
});
