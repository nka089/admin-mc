import { Store } from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";
import { createService, deleteService, updateService } from "../processes/ServicesProcesses";
import { Service } from "../typings/types";
import ServicesView, { ServicesViewProperties } from "../widgets/views/servicesView/ServicesView";

function getProperties(store: Store<State>): ServicesViewProperties {
    const { get, path } = store;
    return {
        services: get(path("services")) || [],
        updateService: (service: Service) => updateService(store, service),
        createService: (service: Service) => createService(store, service),
        deleteService: (id: string) => deleteService(store, id)
    };
}

export default StoreContainer(ServicesView, "state", {
    paths: path => [path("services")],
    getProperties
});
