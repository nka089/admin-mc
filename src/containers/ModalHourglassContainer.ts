import { Store } from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";
import ModalHourglass, { ModalHourGlassProperties } from "../widgets/commons/hourglass/ModalHourglass";

function getProperties(store: Store<State>): ModalHourGlassProperties {
    const { get, path } = store;
    return {
        show: get(path("control", "showHourglass"))
    };
}

export default StoreContainer(ModalHourglass, "state", {
    paths: path => [path("control", "showHourglass")],
    getProperties
});
