import FurnitureView, { FurnitureViewProperties } from "../widgets/views/furnitureView/FurnitureView";
import { Store } from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import StoreContainer from "./StoreContainer";
import { Item } from "../typings/types";
import { deleteItem, createItem, updateItem } from "../processes/ItemsProcesses";

function getProperties(store: Store<State>): FurnitureViewProperties {
    const { get, path } = store;
    return {
        categories: get(path("categories")) || [],
        items: get(path("items")) || [],
        createItem: (item: Item) => createItem(store, item),
        updateItem: (item: Item) => updateItem(store, item),
        deleteItem: (id: string) => deleteItem(store, id)
    };
}

export default StoreContainer(FurnitureView, "state", {
    paths: path => [path("categories"), path("items")],
    getProperties
});
