import { DEV_LOCAL } from "../constants/Constants";

declare var UMCONFUrls: any;
export default class HTTPRequest {
    //@ts-ignore ignore unused private field
    private hostUrl?: string;

    constructor(hostUrl?: string) {
        if (DEV_LOCAL() && !hostUrl) {
            this.hostUrl = "https://ummei.dev1--server.de";
            // this.hostUrl = "https://ummei.dev1--server.de";
        } else {
            this.hostUrl = hostUrl;
        }
    }

    /**
     *
     * @param path rest path with parameters
     * @throws rest error
     */
    public async getRequest(path: string): Promise<any> {
        return this.request(REQUEST_METHODS.GET, path);
    }

    /**
     *
     * @param path rest path with parameters
     * @throws rest error
     */
    public async deleteRequest(path: string): Promise<any> {
        return this.request(REQUEST_METHODS.DELETE, path);
    }

    /**
     *
     * @param path rest path with parameters
     * @param body body
     * @throws rest error
     */
    public async postRequest(path: string, body?: any): Promise<any> {
        return this.request(REQUEST_METHODS.POST, path, body);
    }

    /**
     *
     * @param path rest path with parameters
     * @param body body
     * @throws rest error
     */
    public async putRequest(path: string, body?: any): Promise<any> {
        return this.request(REQUEST_METHODS.PUT, path, body);
    }

    private async request(method: REQUEST_METHODS, path: string, body?: any): Promise<any> {
        if (this.hostUrl) {
            path = this.hostUrl.concat(path);
        }
        const timeout = 25000;
        return new Promise<any>((resolve, reject) => {
            const request = new XMLHttpRequest();

            request.timeout = timeout;
            request.open(method, path);

            // request.withCredentials = true;
            if (UMCONFUrls) {
                request.setRequestHeader("X-WP-NONCE", UMCONFUrls.nonce);
            }

            request.ontimeout = () => {
                return reject(this.createErrorFromRequest(request));
            };

            if (body) {
                request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
                request.send(JSON.stringify(body));
            } else {
                request.send();
            }
            request.onload = () => {
                if (request.status === 200 || request.status === 204 || request.status === 201) {
                    try {
                        const json = JSON.parse(request.responseText);
                        if (typeof json === "object") {
                            return resolve(json);
                        }
                    } catch (jsonParseError) {
                        // string, number or xml -> ignore parsing error
                        // NOPE
                    }
                    return resolve(request.responseText);
                } else {
                    body ? console.log("Request payload:", body) : () => {};

                    return reject(this.createErrorFromRequest(request));
                }
            };

            request.onerror = () => {
                body ? console.log("Request payload:", body) : () => {};
                return reject(this.createErrorFromRequest(request));
            };
        });
    }

    private createErrorFromRequest(request: XMLHttpRequest): RestError {
        return <RestError>{
            status: request.status,
            statustext: request.statusText,
            responseURL: request.responseURL,
            responseText: request.responseText
        };
    }
}

enum REQUEST_METHODS {
    GET = "GET",
    PUT = "PUT",
    POST = "POST",
    DELETE = "DELETE"
}

interface RestError {
    status?: number;
    statustext?: string;
    responseURL?: string;
    responseText?: string;
}
