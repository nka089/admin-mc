const BASE = "/wp-json/um-configurator/v1";

export namespace ServicePaths {
    //#region shared
    export const ITEM_ALL = `${BASE}/item/all`;
    export const OPTIONS = (name: string) => `${BASE}/options/${name}`;
    export const ORDER = `${BASE}/order`;
    export const SERVICE_ALL = `${BASE}/service/all`;
    export const ALL_OPTIONS = `${BASE}/options`;
    //#endregion

    //#region fe-only
    export const INPUT_INFOS = `${BASE}/inputinformations`;
    //#endregion

    //#region be only
    export const CATEGORY = `${BASE}/item-category`;
    export const CATEGORY_ALL = `${BASE}/item-category/all`;
    export const CATEGORY_BY_ID = (id: string) => `${BASE}/item-category/${id}`;
    export const ITEM = `${BASE}/item`;
    export const ITEM_BY_ID = (id: string) => `${BASE}/item/${id}`;
    export const LAST_ORDERS = (numberOfOrders: string) => `${BASE}/order/all?posts_per_page=${numberOfOrders}&paged=1&order=DESC&orderby=ID`;
    export const ORDER_BY_ID = (id: string) => `${BASE}/order/${id}`;
    export const SEARCH_ORDER_BY_DATE_OR_NAME = (dateOrName: string) => `${BASE}/order/all?s=${dateOrName}`;
    export const SERVICE = `${BASE}/service`;
    export const SERVICE_BY_ID = (id: string) => `${BASE}/service/${id}`;
    //#endregion
}
