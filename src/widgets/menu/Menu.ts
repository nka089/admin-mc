import { DNode } from "@dojo/framework/core/interfaces";
import { create, w } from "@dojo/framework/core/vdom";
import Link from "@dojo/framework/routing/ActiveLink";
import Toolbar from "@dojo/widgets/toolbar";
import * as css from "./menu.m.css";

const factory = create();

export default factory(function Menu(): DNode {
    return w(Toolbar, { extraClasses: { root: css.toolbar }, heading: "Umzug-Meister", collapseWidth: 800 }, [
        w(
            Link,
            {
                to: "auftraege",
                isOutlet: true,
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Aufträge"]
        ),
        w(
            Link,
            {
                to: "leistungen",
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Leistungen"]
        ),
        w(
            Link,
            {
                to: "moebel",
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Möbel"]
        ),
        w(
            Link,
            {
                to: "kategorien",
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Kategorien"]
        ),
        w(
            Link,
            {
                to: "optionen",
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Optionen"]
        ),
        w(
            Link,
            {
                to: "analytics",
                classes: [css.link],
                activeClasses: [css.selected]
            },
            ["Analytics"]
        )
    ]);
});
