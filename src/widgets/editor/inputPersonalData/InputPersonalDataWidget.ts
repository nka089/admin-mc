import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { DATA_PAYLOAD_KEYS } from "src/typings/payloads";
import { Order } from "src/typings/types";
import Input from "@dojo/widgets/text-input";
import Select from "@dojo/widgets/select";
import * as css from "./inputPersonalDataWidget.m.css";
import * as global from "../styles.m.css";
import TitlePane from "@dojo/widgets/title-pane";
import watch from "@dojo/framework/core/decorators/watch";

export interface InputPersonalDataWidgetProperties {
    order: Order;
    setData: (key: string, data: string) => void;
}

export class InputPersonalDataWidget extends WidgetBase<InputPersonalDataWidgetProperties> {
    @watch() private open = false;

    protected render(): DNode {
        const { setData, order } = this.properties;
        const anrede = v("div", { classes: global.inputWrapper }, [
            w(Select, {
                label: "Anrede",
                value: order.customer.salutation,
                onChange: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.salutation, value);
                },
                options: ["Frau", "Herr"]
            })
        ]);

        const fistName = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Vorname",
                value: order.customer.firstName,
                onBlur: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.firstName, value.trim());
                }
            })
        ]);
        const lastName = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Nachname",
                value: order.customer.lastName,
                onBlur: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.lastName, value.trim());
                }
            })
        ]);

        const company = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Firma",
                value: order.customer.company,
                onBlur: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.company, value.trim());
                }
            })
        ]);

        const tel = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Telefon",
                value: order.customer.telNumber,
                onBlur: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.telNumber, value.trim());
                }
            })
        ]);

        const email = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "E-Mail",
                value: order.customer.email,
                onBlur: (value: string) => {
                    setData(DATA_PAYLOAD_KEYS.email, value.trim());
                }
            })
        ]);

        const div = v("div", { classes: global.flexContainer }, [
            v("div", { classes: global.container }, [anrede, fistName, lastName]),
            v("div", { classes: global.container }, [company, tel, email])
        ]);

        const personalAcc = w(
            TitlePane,
            {
                open: this.open,
                onRequestClose: () => {
                    this.open = false;
                },
                onRequestOpen: () => {
                    this.open = true;
                },
                title: "Kundendaten"
            },
            [div]
        );

        return v("div", { classes: css.main }, [personalAcc]);
    }
}
