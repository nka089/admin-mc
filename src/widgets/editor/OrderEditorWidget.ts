import { v, w } from "@dojo/framework/core/vdom";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { watch } from "@dojo/framework/core/decorators/watch";
import { always } from "@dojo/framework/core/diff";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Button from "@dojo/widgets/button";
import Dialog from "@dojo/widgets/dialog";
import Select from "@dojo/widgets/select";
import TextInput from "@dojo/widgets/text-input";
import { to } from "src/main";
import { Item, Order, Service, TimeBasedPrice } from "src/typings/types";
import H4 from "../commons/h4/H4";
import BasicDataWidget from "./basicDataWidget/BasicDataWidget";
import DisposalWidget from "./disposal/DisposalWidget";
import FlatsWidget from "./flats/FlatsWidget";
import { InputPersonalDataWidget } from "./inputPersonalData/InputPersonalDataWidget";
import ItemGridWidget from "./itemGrid/ItemGridWidget";
import MessageWidget from "./message/MessageWidget";
import * as css from "./orderEditorWidget.m.css";
import ServicGridWidget from "./servicesGrid/ServiceGridWidget";
import * as global from "./styles.m.css";
import Icon from "@dojo/widgets/icon";
import { editFunction } from "src/containers/GridContainer";

const Base = I18nMixin(WidgetBase);

export interface OrderEditorProperties {
    order: Order;
    allItems: Item[];
    allServices: Service[];
    update: (order: Order) => void;
    calculate: (order: Order) => void;
    restore: (order: Order) => void;
    delete: () => void;
    createPDF: (order: Order, isTime?: boolean) => void;
    setData: (key: string, data: string) => void;
    setText: (text: string) => void;
    setDisposalText: (text: string) => void;
    setShowDialog: (show: boolean) => void;
    showDialog: boolean;
}

export default class OrderEditor extends Base<OrderEditorProperties> {
    private order: Order = <Order>{};

    @watch() private unsavedChanges = false;

    protected render(): DNode {
        this.order = this.properties.order;
        if (typeof this.order.id === "undefined") {
            this.unsavedChanges = true;
        }
        const components: DNode[] = [];

        //Operations
        components.push(
            w(OrderOperations, {
                order: this.order,
                unsavedChanges: this.unsavedChanges,
                calculate: () => {
                    this.properties.calculate(this.order);
                },
                restore: () => {
                    this.properties.restore(this.order);
                    this.unsavedChanges = false;
                },
                update: () => {
                    this.properties.update(this.order);
                    this.unsavedChanges = false;
                },
                showDialog: () => {
                    this.properties.setShowDialog(true);
                },
                delete: () => {
                    this.properties.delete();
                },
                createPDF: () => {
                    this.properties.createPDF(this.order);
                },
                // archiv: () => {
                //     this.order.archived = true;
                //     this.properties.update(this.order);
                //     to(OutletPaths.ORDERS);
                // },
                copy: () => {
                    const order = { ...this.order };
                    order.id = undefined;
                    order.customer.email = "";

                    order.lupd = order.creationTime = new Date().toLocaleString();
                    editFunction(order);
                    to("auftrag-bearbeiten");
                }
            })
        );
        const { setData, setText, setDisposalText, allItems, allServices } = this.properties;
        const personalData = w(InputPersonalDataWidget, {
            order: this.order,
            setData: (key: string, data: string) => {
                setData(key, data);
                this.setUnsaved();
            }
        });
        const flatsWidget = w(FlatsWidget, {
            order: this.order,
            updateOrder: (order: Order) => {
                this.order = order;
                this.unsavedChanges = true;
            }
        });
        const baseDataWidget = w(BasicDataWidget, {
            order: this.order,
            updateOrder: (order: Order) => {
                this.order = order;
                this.unsavedChanges = true;
            }
        });

        const title = v("div", { classes: [css.title] }, [
            w(H4, {
                text: `ID: ${this.order.id} - Name: ${this.order.customer.salutation || ""} ${this.order.customer.firstName}  ${this.order.customer.lastName}`
            })
        ]);
        const knownIds: Set<string> = new Set<string>();
        allItems.forEach(i => knownIds.add(i.id));
        const unknowItems = this.order.items.filter(item => !knownIds.has(item.id));

        unknowItems.push(...allItems);
        unknowItems.sort((i1, i2) => {
            if (i1.name < i2.name) {
                return -1;
            }
            if (i1.name > i2.name) {
                return 1;
            }
            return 0;
        });
        unknowItems.sort((i1, i2) => {
            if (i2.categories[0] < i1.categories[0]) {
                return -1;
            }
            if (i2.categories[0] > i1.categories[0]) {
                return 1;
            }
            return 0;
        });
        unknowItems.sort((i1, i2) => {
            return parseInt(i2.colli) - parseInt(i1.colli);
        });
        const furnitureGrid = w(ItemGridWidget, {
            order: this.order,

            allItems: unknowItems,
            updateOrder: (order: Order) => {
                this.order = order;
                this.unsavedChanges = true;
            }
        });

        knownIds.clear();
        allServices.forEach(i => knownIds.add(i.id));
        const unknowServices = this.order.services.filter(serv => !knownIds.has(serv.id));
        unknowServices.push(...allServices);

        const servicesGrid = w(ServicGridWidget, {
            order: this.order,
            allservices: unknowServices,
            updateOrder: (order: Order) => {
                this.order = order;
                this.unsavedChanges = true;
                this.invalidate();
            }
        });

        const messageAcc = w(MessageWidget, {
            order: this.order,
            setText: (text: string) => {
                setText(text);
                this.setUnsaved();
            }
        });

        const dispWidget = w(DisposalWidget, {
            order: this.order,
            setDisposalText: (text: string) => {
                setDisposalText(text);
                this.setUnsaved();
            }
        });
        components.push(v("div", { classes: css.widget }, [title]));
        components.push(v("div", { classes: css.widget }, [baseDataWidget]));
        components.push(v("div", { classes: css.widget }, [messageAcc]));
        components.push(v("div", { classes: css.widget }, [personalData]));
        components.push(v("div", { classes: css.widget }, [flatsWidget]));
        components.push(v("div", { classes: css.widget }, [furnitureGrid]));
        components.push(v("div", { classes: css.widget }, [servicesGrid]));
        components.push(v("div", { classes: css.widget }, [dispWidget]));
        components.push(this.dialog());

        const widget = v("div", { classes: [css.main] }, components);
        return widget;
    }

    private setUnsaved(): void {
        this.unsavedChanges = true;
    }

    private dialog(): DNode {
        return w(
            Dialog,
            {
                open: this.properties.showDialog,
                modal: true,
                title: "Angebot auf Stundenbasis erstellen",
                underlay: true,
                onRequestClose: () => {
                    this.properties.setShowDialog(false);
                }
            },
            [
                v("div", { classes: css.d_content }, [
                    v("div", [
                        v("div", { classes: global.inputWrapper }, [
                            w(Select, {
                                label: "Stundenanzahl",
                                value: this.order.timeBased ? this.order.timeBased.hours : "",
                                options: ["0", "2", "3", "4", "5", "6", "7", "8"],
                                onChange: (value: string) => {
                                    let tbp = this.order.timeBased || <TimeBasedPrice>{};
                                    tbp.hours = value;
                                    this.order.timeBased = tbp;
                                    this.setUnsaved();
                                    this.invalidate();
                                }
                            })
                        ]),
                        v("div", { classes: global.inputWrapper }, [
                            w(TextInput, {
                                label: "Stundenpreis",
                                type: "number",
                                value: this.order.timeBased ? this.order.timeBased.extra : "",
                                onBlur: (value: string) => {
                                    let tbp = this.order.timeBased || <TimeBasedPrice>{};
                                    tbp.extra = value;
                                    this.order.timeBased = tbp;
                                    this.setUnsaved();
                                    this.invalidate();
                                }
                            })
                        ]),
                        v("div", { classes: global.inputWrapper }, [
                            w(TextInput, {
                                label: "Gesamtbetrag",
                                type: "number",
                                value: this.order.timeBased ? this.order.timeBased.basis : "",
                                onBlur: (value: string) => {
                                    let tbp = this.order.timeBased || <TimeBasedPrice>{};
                                    tbp.basis = value;
                                    this.order.timeBased = tbp;
                                    this.setUnsaved();
                                    this.invalidate();
                                }
                            })
                        ])
                    ]),
                    v("div", { classes: css.buttons }, [
                        v("div", { classes: css.op_btnWrapper }, [
                            w(
                                Button,
                                {
                                    extraClasses: { root: css.cancel },
                                    onClick: () => {
                                        this.properties.setShowDialog(false);
                                    }
                                },
                                [w(Icon, { extraClasses: { root: css.icon }, type: "closeIcon" }), "Abbrechen"]
                            )
                        ]),

                        v("div", { classes: css.op_btnWrapper }, [
                            w(
                                Button,
                                {
                                    onClick: () => {
                                        this.properties.createPDF(this.order, true);
                                        this.properties.setShowDialog(false);
                                    }
                                },
                                [w(Icon, { extraClasses: { root: css.icon }, type: "checkIcon" }), " PDF Erstellen"]
                            )
                        ])
                    ])
                ])
            ]
        );
    }
}

interface OrderOperationsProperties {
    order: Order;
    unsavedChanges: boolean;
    update: () => void;
    delete: () => void;
    calculate: () => void;
    restore: () => void;
    createPDF: () => void;
    // archiv: () => void;
    showDialog: () => void;
    copy: () => void;
}

class OrderOperations extends WidgetBase<OrderOperationsProperties> {
    @diffProperty("unsavedChanges", always)
    protected onchange(): void {
        this.invalidate();
    }
    protected render(): DNode {
        const copy = w(
            Button,
            {
                disabled: typeof this.properties.order.id === "undefined",

                onClick: () => {
                    this.properties.copy();
                }
            },
            ["Kopieren"]
        );

        const delButton = w(
            Button,
            {
                extraClasses: { root: css.op_delButton },
                disabled: typeof this.properties.order.id === "undefined",
                onClick: () => {
                    if (confirm("Auftrag endgültig entfernen?") === true) {
                        this.properties.delete();
                    }
                }
            },
            ["Löschen"]
        );

        const updateButton = w(
            Button,
            {
                extraClasses: this.properties.unsavedChanges ? { root: css.op_saveButton } : { root: css.button },
                onClick: () => {
                    this.properties.update();
                }
            },
            ["Speichern"]
        );

        const calculateButton = w(
            Button,
            {
                extraClasses: { root: css.button },
                onClick: () => {
                    this.properties.calculate();
                }
            },
            ["Berechnen"]
        );

        const restoreButton = w(
            Button,
            {
                disabled: typeof this.properties.order.id === "undefined",
                extraClasses: { root: css.button },

                onClick: () => {
                    this.properties.restore();
                }
            },
            ["Wiederherstellen"]
        );

        const festpreisButton = w(
            Button,
            {
                disabled: typeof this.properties.order.id === "undefined",
                extraClasses: { root: css.button },

                onClick: () => {
                    this.properties.createPDF();
                }
            },
            ["PDF-Fetspreis"]
        );

        const stundenButton = v("div", [
            w(
                Button,
                {
                    disabled: typeof this.properties.order.id === "undefined",
                    extraClasses: { root: css.button },
                    onClick: () => {
                        this.properties.showDialog();
                    }
                },
                ["PDF-Stunden"]
            )
        ]);
        const nodes = [
            v("div", { classes: css.op_btnWrapper }, [copy]),
            v("div", { classes: css.op_btnWrapper }, [delButton]),
            v("div", { classes: css.op_btnWrapper }, [calculateButton]),
            v("div", { classes: css.op_btnWrapper }, [restoreButton]),
            v("div", { classes: css.op_btnWrapper }, [updateButton]),
            v("div", { classes: css.op_btnWrapper }, [festpreisButton]),
            v("div", { classes: css.op_btnWrapper }, [stundenButton])
        ];

        return v("div", { classes: css.op_main }, nodes);
    }
}
