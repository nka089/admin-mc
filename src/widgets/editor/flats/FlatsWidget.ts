import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Checkbox, { Mode } from "@dojo/widgets/checkbox";
import Icon from "@dojo/widgets/icon";
import Select from "@dojo/widgets/select";
import Input from "@dojo/widgets/text-input";
import translations from "src/nls/translations";
import { Order, Address } from "src/typings/types";
import H5 from "src/widgets/commons/h5/H5";
import * as global from "../styles.m.css";
import TitlePane from "@dojo/widgets/title-pane";
import watch from "@dojo/framework/core/decorators/watch";

const FlatsBase = I18nMixin(WidgetBase);

export interface FlatsProperties {
    order: Order;
    updateOrder: (order: Order) => void;
}

export default class FlatsWidget extends FlatsBase<FlatsProperties> {
    @watch() private open = false;

    private movementObjectsDropdown: string[] = [];
    private areasDropdown: string[] = [];
    private liftsDropdown: string[] = [];
    private floorsDropdown: string[] = [];
    private distancesDropdown: string[] = [];
    private order = <Order>{};

    protected render(): DNode {
        return w(
            TitlePane,
            {
                open: this.open,
                onRequestClose: () => {
                    this.open = false;
                },
                onRequestOpen: () => {
                    this.open = true;
                },
                title: "Wohnungsdaten"
            },
            [this.renderBlock()]
        );
    }

    protected renderBlock(): DNode {
        const { order } = this.properties;
        this.order = order;
        this.movementObjectsDropdown = this._createMovementObjects();
        this.areasDropdown = this._createAreas();
        this.liftsDropdown = this._createLifts();

        this.floorsDropdown = this._createFloors();
        this.distancesDropdown = this._createDistances();

        const { messages } = this.localizeBundle(translations);

        const from: DNode = v("div", { classes: [global.container] }, [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "locationIcon" }), w(H5, { text: `Auszug` })]),

            v("div", { class: global.inputWrapper }, [
                w(Input, {
                    label: messages.address,
                    value: this.order.from.address,
                    onBlur: (value: string) => {
                        if (this.order.from) {
                            this.order.from.address = value;
                        } else {
                            this.order.from = <Address>{ address: value };
                        }

                        this.updateOrder();
                    }
                })
            ]),

            v("div", { class: global.inputWrapper }, [
                w(Select, {
                    label: messages.movementObject,
                    value: order.from.movementObject,
                    onChange: (value: string) => {
                        if (this.order.from) {
                            this.order.from.movementObject = value;
                        } else {
                            this.order.from = <Address>{ movementObject: value };
                        }
                        this.updateOrder();
                    },
                    options: this.movementObjectsDropdown
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Select, {
                    label: messages.area,
                    value: this.order.from.area,
                    onChange: (value: string) => {
                        if (this.order.from) {
                            this.order.from.area = value;
                        } else {
                            this.order.from = <Address>{ area: value };
                        }
                        this.updateOrder();
                    },
                    options: this.areasDropdown
                })
            ]),

            v("div", { class: global.inputWrapper }, [
                w(Select, {
                    label: messages.floor,
                    value: this.order.from.floor,
                    onChange: (value: string) => {
                        if (this.order.from) {
                            this.order.from.floor = value;
                        } else {
                            this.order.from = <Address>{ floor: value };
                        }
                        this.updateOrder();
                    },
                    options: this.floorsDropdown
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Select, {
                    label: messages.lift,
                    value: this.order.from.liftType,
                    onChange: (value: string) => {
                        if (this.order.from) {
                            this.order.from.liftType = value;
                        } else {
                            this.order.from = <Address>{ liftType: value };
                        }
                        this.updateOrder();
                    },
                    options: this.liftsDropdown
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Select, {
                    label: messages.distanceBetween,
                    value: this.order.from.runningDistance,
                    onChange: (value: string) => {
                        if (this.order.from) {
                            this.order.from.runningDistance = value;
                        } else {
                            this.order.from = <Address>{ runningDistance: value };
                        }
                        this.updateOrder();
                    },
                    options: this.distancesDropdown
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Checkbox, {
                    label: "Dachboden umziehen?",
                    mode: Mode.toggle,
                    checked: this.order.from.hasLoft,
                    onChange: (value: string, checked: boolean) => {
                        if (this.order.from) {
                            this.order.from.hasLoft = checked;
                        } else {
                            this.order.from = <Address>{ hasLoft: checked };
                        }
                        this.updateOrder();
                    }
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Checkbox, {
                    label: "Einpackservice?",
                    mode: Mode.toggle,
                    checked: this.order.from.packservice,
                    onChange: (value: string, checked: boolean) => {
                        if (this.order.from) {
                            this.order.from.packservice = checked;
                        } else {
                            this.order.from = <Address>{ packservice: checked };
                        }
                        this.updateOrder();
                    }
                })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Checkbox, {
                    label: "Halteverbotszone",
                    mode: Mode.toggle,
                    checked: this.order.from.parkingSlot,
                    onChange: (value: string, checked: boolean) => {
                        if (this.order.from) {
                            this.order.from.parkingSlot = checked;
                        } else {
                            this.order.from = <Address>{ parkingSlot: checked };
                        }
                        this.updateOrder();
                    }
                })
            ])
        ]);

        const to = v("div", { classes: [global.container, global.last] }, [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "locationIcon" }), w(H5, { text: `Einzug` })]),
            v("div", [
                v("div", { class: global.inputWrapper }, [
                    w(Input, {
                        label: messages.address,
                        value: this.order.to.address,
                        onBlur: (value: string) => {
                            if (this.order.to) {
                                this.order.to.address = value;
                            } else {
                                this.order.to = <Address>{ address: value };
                            }
                            this.updateOrder();
                        }
                    })
                ]),
                v("div", { class: global.inputWrapper }, [
                    w(Select, {
                        label: messages.movementObject,
                        value: order.to.movementObject,
                        onChange: (value: string) => {
                            if (this.order.to) {
                                this.order.to.movementObject = value;
                            } else {
                                this.order.to = <Address>{ movementObject: value };
                            }
                            this.updateOrder();
                        },
                        options: this.movementObjectsDropdown
                    })
                ]),
                v("div", { styles: { visibility: "hidden" } }, [v("div", { class: global.inputWrapper }, [w(Input, { label: "i" })])]),
                v("div", { class: global.inputWrapper }, [
                    w(Select, {
                        label: messages.floor,
                        value: this.order.to.floor,
                        onChange: (value: string) => {
                            if (this.order.to) {
                                this.order.to.floor = value;
                            } else {
                                this.order.to = <Address>{ floor: value };
                            }
                            this.updateOrder();
                        },
                        options: this.floorsDropdown
                    })
                ]),
                v("div", { class: global.inputWrapper }, [
                    w(Select, {
                        label: messages.lift,
                        value: this.order.to.liftType,
                        onChange: (value: string) => {
                            if (this.order.to) {
                                this.order.to.liftType = value;
                            } else {
                                this.order.to = <Address>{ liftType: value };
                            }
                            this.updateOrder();
                        },
                        options: this.liftsDropdown
                    })
                ]),

                v("div", { class: global.inputWrapper }, [
                    w(Select, {
                        label: messages.distanceBetween,
                        value: this.order.to.runningDistance,
                        onChange: (value: string) => {
                            if (this.order.to) {
                                this.order.to.runningDistance = value;
                            } else {
                                this.order.to = <Address>{ runningDistance: value };
                            }
                            this.updateOrder();
                        },
                        options: this.distancesDropdown
                    })
                ]),
                v("div", { class: global.inputWrapper }, [
                    w(Checkbox, {
                        label: "Dachboden umziehen?",
                        mode: Mode.toggle,
                        checked: this.order.to.hasLoft,
                        onChange: (value: string, checked: boolean) => {
                            if (this.order.to) {
                                this.order.to.hasLoft = checked;
                            } else {
                                this.order.to = <Address>{ hasLoft: checked };
                            }
                            this.updateOrder();
                        }
                    })
                ]),
                v("div", { class: global.inputWrapper }, [
                    w(Checkbox, {
                        label: "Auspackservice?",
                        mode: Mode.toggle,
                        checked: this.order.to.packservice,
                        onChange: (value: string, checked: boolean) => {
                            if (this.order.to) {
                                this.order.to.packservice = checked;
                            } else {
                                this.order.to = <Address>{ packservice: checked };
                            }
                            this.updateOrder();
                        }
                    })
                ]),
                v("div", { class: global.inputWrapper }, [
                    w(Checkbox, {
                        label: "Halteverbotszone",
                        mode: Mode.toggle,
                        checked: this.order.to.parkingSlot,
                        onChange: (value: string, checked: boolean) => {
                            if (this.order.to) {
                                this.order.to.parkingSlot = checked;
                            } else {
                                this.order.to = <Address>{ parkingSlot: checked };
                            }
                            this.updateOrder();
                        }
                    })
                ])
            ])
        ]);

        const middle = v("div", { classes: [global.container] }, [
            v("div", { classes: global.title }, [
                w(Icon, { extraClasses: { root: global.green }, type: "leftIcon" }),
                w(Icon, { extraClasses: { root: global.green }, type: "rightIcon" }),
                w(H5, { text: `Distanz` })
            ]),
            v("div", { class: global.inputWrapper }, [
                w(Input, {
                    label: "Gesamtfahrstrecke",
                    value: String(this.order.distance),
                    onBlur: (value: any) => {
                        this.order.distance = parseFloat(value);
                        this.updateOrder();
                    }
                })
            ])
        ]);

        return v("div", { classes: [global.flexContainer] }, [from, middle, to]);
    }

    private updateOrder(): void {
        this.properties.updateOrder(this.order);
        this.invalidate();
    }

    private _createMovementObjects(): string[] {
        const { messages } = this.localizeBundle(translations);
        return ["-", messages.house, messages.flat, messages.basement, messages.lager, messages.office, messages.garden];
    }

    private _createAreas(): string[] {
        const areas: string[] = ["-"];
        for (let i = 10; i < 150; i = i + 10) {
            areas.push(`${i} m²`);
        }
        areas.push(`150+ m²`);
        return areas;
    }

    private _createLifts(): string[] {
        const { messages } = this.localizeBundle(translations);
        return ["-", messages.noLift, messages.lift2, messages.lift4, messages.lift6, messages.lift8];
    }

    private _createFloors(): string[] {
        const { messages } = this.localizeBundle(translations);

        const floors: string[] = ["-", "EG"];
        for (let i = 1; i < 9; i++) {
            floors.push(`${i}. ${messages.floor}`);
        }
        floors.push(`9+ ${messages.floor}`);
        return floors;
    }

    private _createDistances(): string[] {
        const distances: string[] = ["-"];
        for (let i = 0; i < 100; i = i + 10) {
            distances.push(`${i} m.`);
        }
        distances.push(`100+ m.`);
        return distances;
    }
}
