import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { Order } from "src/typings/types";
import { Utils } from "src/utils/Utils";
import Input from "@dojo/widgets/text-input";
import TimePicker from "@dojo/widgets/time-picker";
import * as css from "./basicDataWidget.m.css";
import * as global from "../styles.m.css";
import H5 from "src/widgets/commons/h5/H5";
import Icon from "@dojo/widgets/icon";

export interface BasicDataWidgetProperties {
    order: Order;
    updateOrder: (order: Order) => void;
}
export default class BasicDataWidget extends WidgetBase<BasicDataWidgetProperties> {
    private order: Order = <Order>{};
    protected render(): DNode {
        const { order } = this.properties;
        this.order = order;

        const dateInput = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Datum",
                value: this.order.isDateFix ? this.order.date : `${this.order.date_from} - ${this.order.date_to}`,
                onBlur: (value: any) => {
                    this.order.date = value;
                    this.order.isDateFix = true;
                    this.updateOrder();
                }
            })
        ]);

        const manualCbmInput = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Extra m³",
                type: "number",
                value: this.order.extraCbm,
                onBlur: (value: string) => {
                    let extraCbm = value.trim();
                    extraCbm = Utils.replaceAll(extraCbm, ",", ".");
                    this.order.extraCbm = extraCbm;
                    this.updateOrder();
                }
            })
        ]);

        const discountInput = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Extra €",
                type: "number",
                value: this.order.discount,
                onBlur: (value: string) => {
                    let discount = value.trim();
                    discount = Utils.replaceAll(discount, ",", ".");
                    this.order.discount = discount;
                    this.updateOrder();
                }
            })
        ]);

        const t35Input = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "LKW 3,5",
                type: "number",
                value: typeof this.order.transporterNumber === "number" ? this.order.transporterNumber.toString() : "",
                onBlur: (value: string) => {
                    this.order.transporterNumber = parseInt(value);
                    this.updateOrder();
                }
            })
        ]);

        const t75Input = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "LKW 7,5",
                type: "number",
                value: typeof this.order.t75 === "number" ? this.order.t75.toString() : "",
                onBlur: (value: string) => {
                    this.order.t75 = parseInt(value);
                    this.updateOrder();
                }
            })
        ]);

        const workerInput = v("div", { classes: global.inputWrapper }, [
            w(Input, {
                label: "Ladehelfer",
                type: "number",

                value: typeof this.order.workersNumber === "number" ? this.order.workersNumber.toString() : "",
                onBlur: (value: string) => {
                    this.order.workersNumber = parseInt(value);
                    this.updateOrder();
                }
            })
        ]);

        const timeInput = v("div", { classes: global.inputWrapper }, [
            w(TimePicker, {
                extraClasses: { dropdown: css.drop },
                label: "Uhrzeit",
                value: this.order.time,
                end: "18:00",
                start: "05:00",
                step: 1800,
                useNativeElement: false,
                clearable: false,
                onChange: (value: string) => {
                    this.order.time = value;
                    this.updateOrder();
                }
            })
        ]);

        const left = v("div", { classes: global.container }, [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "dateIcon" }), w(H5, { text: `Umzugsdatum` })]),
            dateInput,
            timeInput
        ]);
        const middle = v("div", { classes: global.container }, [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "linkIcon" }), w(H5, { text: `Ressourcen` })]),
            t35Input,
            t75Input,
            workerInput
        ]);
        const right = v("div", { classes: [global.container, global.last] }, [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "settingsIcon" }), w(H5, { text: `Anpassungen` })]),
            discountInput,
            manualCbmInput
        ]);

        return v("div", { classes: global.flexContainer }, [left, middle, right]);
    }

    private updateOrder(): void {
        this.properties.updateOrder(this.order);
        this.invalidate();
    }
}
