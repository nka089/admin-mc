import { v, w } from "@dojo/framework/core/vdom";
import watch from "@dojo/framework/core/decorators/watch";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import TitlePane from "@dojo/widgets/title-pane";
import { Order, Service } from "src/typings/types";
import Input from "@dojo/widgets/text-input";
import * as css from "./serviceGridwidget.m.css";

interface ServiceGridProperties {
    allservices: Service[];
    order: Order;
    updateOrder: (order: Order) => void;
}
const Base = I18nMixin(WidgetBase);

export default class ServicGridWidget extends Base<ServiceGridProperties> {
    private services: Service[] = [];
    private order: Order = <Order>{};

    @watch() private openPack = false;
    @watch() private openBohr = false;

    protected render(): DNode {
        const { allservices, order } = this.properties;
        this.order = order;

        this.services = this.order.services;
        const packRows = allservices
            .filter(s => s.tag === "Packmaterial")
            .map(service => {
                return w(ServiceRow, {
                    service,
                    actual: this.services.find(serv => serv.id === service.id),
                    update: (service: Service) => {
                        this.updateService(service);
                    }
                });
            });
        const bohrRows = allservices
            .filter(s => s.tag === "Bohrarbeiten")
            .map(service => {
                return w(ServiceRow, {
                    service,
                    actual: this.services.find(serv => serv.id === service.id),
                    update: (service: Service) => {
                        this.updateService(service);
                    }
                });
            });
        const head = v("tr", { classes: css.row }, [
            v("th", { classes: css.cell }, ["Bezeichnung"]),
            v("th", { classes: css.cell }, ["Preis"]),
            v("th", { classes: css.cell }, ["Anzahl"])
        ]);

        const packTable = v("table", { classes: css.table }, [v("thead", [head]), v("tbody", packRows)]);
        const bohrTable = v("table", { classes: css.table }, [v("thead", [head]), v("tbody", bohrRows)]);
        return v("div", [
            v("div", { classes: css.pane }, [
                w(
                    TitlePane,
                    {
                        title: `Packmaterial`,
                        open: this.openPack,
                        onRequestClose: () => {
                            this.openPack = false;
                        },
                        onRequestOpen: () => {
                            this.openPack = true;
                        }
                    },
                    [packTable]
                )
            ]),
            w(
                TitlePane,
                {
                    title: `Bohrarbeiten`,
                    open: this.openBohr,
                    onRequestClose: () => {
                        this.openBohr = false;
                    },
                    onRequestOpen: () => {
                        this.openBohr = true;
                    }
                },
                [bohrTable]
            )
        ]);
    }

    private updateService(service: Service): void {
        const idx = this.order.services.findIndex(serv => serv.id === service.id);
        if (idx > -1) {
            this.order.services[idx] = service;
        } else {
            this.order.services.push(service);
        }
        this.properties.updateOrder(this.order);
        this.invalidate();
    }
}

interface ServiceRowProperties {
    service: Service;
    actual: Service | undefined;
    update: (service: Service) => void;
}
class ServiceRow extends Base<ServiceRowProperties> {
    protected render(): DNode {
        const { service, actual, update } = this.properties;
        if (actual) {
            service.colli = actual.colli.trim();
        }

        const row = v(
            "tr",
            {
                classes: [css.row, service.colli && service.colli.length > 0 && service.colli !== "0" ? css.green : null]
            },
            [
                v("td", { title: `Id: ${service.id}`, classes: css.cell }, [service.name]),
                v("td", { classes: css.cell }, [service.price.toString()]),
                v("td", { classes: css.cell }, [
                    w(Input, {
                        type: "number",
                        extraClasses: { root: css.input },
                        onBlur: (value: string) => {
                            service.colli = value.trim();
                            update(service);
                        },
                        value: actual && actual.colli ? actual.colli : ""
                    })
                ])
            ]
        );
        return row;
    }
}
