import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Icon from "@dojo/widgets/icon";
import translations from "src/nls/translations";
import { Order } from "src/typings/types";
import H4 from "src/widgets/commons/h4/H4";
import H5 from "src/widgets/commons/h5/H5";
import * as css from "./pricecalculator.m.css";

export interface PriceCalculatorProperties {
    order: Order;
    sum: string;
    hvzPrice: number;
    // options: number;
}

const PriceCalculatorBase = I18nMixin(WidgetBase);
export default class PriceCalculator extends PriceCalculatorBase<PriceCalculatorProperties> {
    private calcHvz(order: Order): number {
        let p = 0;
        if (order.from.parkingSlot) {
            p += 1;
        }
        if (order.to.parkingSlot) {
            p += 1;
        }
        return p * this.properties.hvzPrice;
    }

    private calcServices(order: Order): number {
        let sum = 0;
        order.services
            .filter(s => s.colli !== "0" && s.colli !== "")
            .forEach(s => {
                let c = parseInt(s.colli);
                let p = parseFloat(s.price);
                sum += c * p;
            });
        return sum;
    }

    protected render(): DNode {
        const { order } = this.properties;

        const { volume, rideCosts, sum, volumePrice, customer, id, prices } = order;
        const { email, telNumber, lastName, salutation } = customer;
        const { messages } = this.localizeBundle(translations);

        const _titel = w(H4, { text: "Kosten" });

        const _volume = v("div", {}, [w(H5, { text: messages.volume }), v("div", { classes: css.value }, [`${volume ? volume : "0"} m³`])]);
        const _volumePrice = v("div", {}, [w(H5, { text: messages.volumePrice }), v("div", { classes: css.value }, [`${volumePrice ? volumePrice : "0"} €`])]);
        const _rideCoasts = v("div", {}, [w(H5, { text: messages.rideCoasts }), v("div", { classes: css.value }, [`${rideCosts} €`])]);
        const _hvz = v("div", {}, [
            w(H5, { text: "Halteverbotszonen" }),
            v("div", { classes: css.value }, [`${prices && prices.halteverbotszonen ? prices.halteverbotszonen : this.calcHvz(order)} €`])
        ]);
        const _services = v("div", {}, [
            w(H5, { text: "Kartons, Bohrarbeiten etc." }),
            v("div", { classes: css.value }, [`${prices && prices.services ? prices.services : this.calcServices(order)} €`])
        ]);
        const _allPrice = v("div", {}, [w(H5, { text: messages.sum }), v("div", { classes: css.value }, [`${sum} €`])]);

        const href = `mailto:${email}?subject=Ihr%20Umzugsanfrage%20${"ID:"}${id}%20bei%20umzug-meister.de&body=Hallo%20${salutation}%20${lastName},`;

        const _email = v("div", { classes: css.contact }, [
            w(Icon, { extraClasses: { root: css.icon }, type: "mailIcon" }),
            v(
                "a",
                {
                    href
                },
                [`${email}`]
            )
        ]);
        const _tel = v("div", { classes: css.contact }, [w(Icon, { extraClasses: { root: css.icon }, type: "phoneIcon" }), v("div", {}, [`${telNumber}`])]);
        const costsBlock = v("div", { classes: css.block }, [
            _titel,
            _volume,
            _volumePrice,
            _rideCoasts,
            _hvz,
            _services,
            v("div", { classes: css.placeholder }),
            _allPrice
        ]);

        const contacts = v("div", { classes: css.block }, [_email, _tel]);

        return v("div", [contacts, costsBlock]);
    }
}
