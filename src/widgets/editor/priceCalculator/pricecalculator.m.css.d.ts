export const block: string;
export const value: string;
export const icon: string;
export const placeholder: string;
export const contact: string;
