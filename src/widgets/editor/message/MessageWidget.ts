import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { v, w } from "@dojo/framework/core/vdom";
import TextArea from "@dojo/widgets/text-area";
import { Order } from "src/typings/types";
import * as global from "../styles.m.css";
import Icon from "@dojo/widgets/icon";
import H5 from "src/widgets/commons/h5/H5";

import * as css from "./messageWidget.m.css";

interface MessageWidgetProerties {
    order: Order;
    setText: (text: string) => void;
}

export default class MessageWidget extends WidgetBase<MessageWidgetProerties> {
    protected render(): DNode {
        const { order, setText } = this.properties;

        const textBemerkungen = v("div", [
            v("div", { classes: global.title }, [w(Icon, { extraClasses: { root: global.green }, type: "editIcon" }), w(H5, { text: `Notizfeld` })]),

            w(TextArea, {
                extraClasses: { input: css.input },
                label: `Kundennachricht / Vereinbarungen`,
                value: order.text,
                onBlur: (value: string) => {
                    setText(value.trim());
                }
            })
        ]);

        const div = v(
            "div",
            {
                styles: { width: "100%", maxWidth: "unset", marginBottom: "50px" },
                classes: global.container
            },
            [textBemerkungen]
        );

        return div;
    }
}
