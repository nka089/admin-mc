import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { v, w } from "@dojo/framework/core/vdom";
import TextArea from "@dojo/widgets/text-area";
import { Order } from "src/typings/types";
import TitlePane from "@dojo/widgets/title-pane";
import watch from "@dojo/framework/core/decorators/watch";
import * as css from "./disposalWidget.m.css";

interface DisposalWidgetProerties {
    order: Order;
    setDisposalText: (text: string) => void;
}

export default class DisposalWidget extends WidgetBase<DisposalWidgetProerties> {
    @watch()
    private open = false;
    protected render(): DNode {
        const { order, setDisposalText } = this.properties;

        const disposalText = v("div", [
            w(TextArea, {
                extraClasses: { input: css.input },
                value: order.disposalText,
                onBlur: (value: string) => {
                    const text = value.trim();
                    setDisposalText(text);
                }
            })
        ]);

        const disposalAcc = w(
            TitlePane,
            {
                open: this.open,
                onRequestClose: () => {
                    this.open = false;
                },
                onRequestOpen: () => {
                    this.open = true;
                },
                title: `Entrümpelung vorhanden: ${order.disposalFlag && order.disposalText && order.disposalText.length > 0 ? "JA" : "NEIN"}`
            },
            [disposalText]
        );
        return disposalAcc;
    }
}
