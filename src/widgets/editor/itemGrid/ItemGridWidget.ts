import { v, w } from "@dojo/framework/core/vdom";
import watch from "@dojo/framework/core/decorators/watch";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import TextInput from "@dojo/widgets/text-input";
import TitlePane from "@dojo/widgets/title-pane";
import translations from "src/nls/translations";
import { Item, Order } from "src/typings/types";
import Checkbox from "@dojo/widgets/checkbox";
import * as css from "./itemGridWidget.m.css";

interface ItemGridProperties {
    allItems: Item[];
    order: Order;
    updateOrder: (order: Order) => void;
}

export default class ItemGridWidget extends WidgetBase<ItemGridProperties> {
    @watch() private open = false;
    private items: Item[] = [];
    private order: Order = <Order>{};

    protected render(): DNode {
        const { allItems, order } = this.properties;
        this.order = order;

        this.items = this.order.items;

        const rows = allItems.map(item =>
            w(ItemRow, {
                item,
                actual: this.items.find(i => i.id === item.id && i.categories[0] === item.categories[0]),
                update: (item: Item) => {
                    this.updateitem(item);
                }
            })
        );
        const head = v("tr", { classes: css.row }, [
            v("th", { classes: css.cell }, ["Zimmer"]),
            v("th", { classes: css.cell }, ["Bezeichnung"]),
            v("th", { classes: css.cell }, ["m³"]),
            v("th", { classes: css.cell }, ["extra €"]),
            v("th", { classes: css.cell }, ["Anzahl"]),
            v("th", { classes: css.cell }, ["Eigenschaften"])
        ]);

        const table = v("table", { classes: css.table }, [v("thead", [head]), v("tbody", rows)]);

        const boxesInput = w(TextInput, {
            label: "Anzahl der Kartons",
            type: "number",
            value: this.order.boxNumber ? this.order.boxNumber.toString() : "0",
            onBlur: (value: any) => {
                this.order.boxNumber = parseInt(value);
                this.updateOrder();
            }
        });

        return w(
            TitlePane,
            {
                open: this.open,
                onRequestClose: () => {
                    this.open = false;
                },
                onRequestOpen: () => {
                    this.open = true;
                },
                title: `Umzugsgutliste: Gesamtvolumen ${order.volume} m³`
            },
            [v("div", { classes: css.boxes }, [boxesInput]), table]
        );
    }

    private updateOrder(): void {
        this.properties.updateOrder(this.order);
        this.invalidate();
    }
    private updateitem(item: Item): void {
        const idx = this.order.items.findIndex(i => i.id === item.id && i.categories[0] === item.categories[0]);
        if (idx > -1) {
            if (item.colli === "0") {
                this.order.items.splice(idx, 1);
            } else {
                this.order.items[idx] = item;
            }
        } else {
            this.order.items.push(item);
        }
        this.properties.updateOrder(this.order);
        this.invalidate();
    }
}

interface ItemRowProperties {
    item: Item;
    actual: Item | undefined;
    update: (item: Item) => void;
}

const Base = I18nMixin(WidgetBase);
//@ts-ignore
class ItemRow extends Base<ItemRowProperties> {
    protected render(): DNode {
        const { messages } = this.localizeBundle(translations);
        const { item, actual, update } = this.properties;
        if (actual) {
            item.colli = actual.colli;
            item.categories = actual.categories;
            item.demontage = actual.demontage;
            item.bulky = actual.bulky;
            item.m100 = actual.m100;
            item.m150 = actual.m150;
            item.montage = actual.montage;
            item.notDismountable = actual.notDismountable;
        }

        const row = v(
            "tr",
            {
                classes: [css.row, item.colli === "0" ? null : css.green]
            },
            [
                v("td", { classes: css.cell }, [item.categories[0]]),
                v("td", { title: `Id: ${item.id}`, classes: css.cell }, [item.name]),
                v("td", { classes: css.cell }, [item.volume.toString()]),
                v("td", { classes: css.cell }, [item.extraPrice ? item.extraPrice : ""]),
                v("td", { classes: css.cell }, [
                    w(TextInput, {
                        type: "number",
                        extraClasses: { root: css.input },
                        onBlur: (value: string) => {
                            item.colli = value.trim();
                            update(item);
                            this.invalidate();
                        },
                        value: actual && actual.colli ? actual.colli : "0"
                    })
                ]),
                v("td", { classes: css.cell }, [
                    v("div", { classes: css.allCheckboxes }, [
                        typeof item.demontage === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.demontage,
                                      checked: actual && actual.demontage,
                                      onChange: (value: string, checked: boolean) => {
                                          item.demontage = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null,

                        typeof item.montage === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.montage,
                                      checked: actual && actual.montage,
                                      onChange: (value: string, checked: boolean) => {
                                          item.montage = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null,

                        typeof item.notDismountable === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.notDismountable,
                                      checked: actual && actual.notDismountable,
                                      onChange: (value: string, checked: boolean) => {
                                          item.notDismountable = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null,
                        typeof item.m100 === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.m100,
                                      checked: actual && actual.m100,
                                      onChange: (value: string, checked: boolean) => {
                                          item.m100 = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null,
                        typeof item.m150 === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.m150,
                                      checked: actual && actual.m150,
                                      onChange: (value: string, checked: boolean) => {
                                          item.m150 = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null,
                        typeof item.bulky === "boolean"
                            ? v("div", { classes: [css.checkBoxDiv] }, [
                                  w(Checkbox, {
                                      label: messages.bulky,
                                      checked: actual && actual.bulky,
                                      onChange: (value: string, checked: boolean) => {
                                          item.bulky = checked;
                                          update(item);
                                          this.invalidate();
                                      }
                                  })
                              ])
                            : null
                    ])
                ])
            ]
        );
        return row;
    }
}
