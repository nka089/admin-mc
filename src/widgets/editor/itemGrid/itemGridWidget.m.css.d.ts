export const cell: string;
export const green: string;
export const input: string;
export const row: string;
export const allCheckboxes: string;
export const checkBoxDiv: string;
export const table: string;
export const boxes: string;
