import Map from "@dojo/framework/shim/Map";
import Store from "@dojo/framework/stores/Store";
import { ColumnConfig } from "@dojo/widgets/grid/interfaces";
import StoreContainer from "src/containers/StoreContainer";
import { ORDER } from "src/processes/OrderProcesses";
import { CONTROL } from "src/processes/Processes";
import { State } from "src/typings/store";
import { setAnalytics, setAnalyticsCounter } from "./AnalyticsProcesses";
import AnalyticsWidget, { AnalyticsApi } from "./AnalyticsWidget";

const columnConfig: ColumnConfig[] = [
    { id: "id", title: "ID", sortable: true },
    { id: "name", title: "Bezeichnung", sortable: true },
    { id: "value", title: "Anzahl", sortable: true }
];

function getProperties(store: Store<State>): AnalyticsApi {
    const { get, path } = store;
    const data = get(path("analytics", "data")) || [];
    let count = get(path("analytics", "counter")) || 0;

    const _loadData = async function(numberOfOrder: number, store: Store<State>): Promise<void> {
        CONTROL.setShowHourlassProcess(store)(true);

        const orders = await ORDER.getLastOrdders(String(numberOfOrder));
        setAnalyticsCounter(store)(orders.length);

        const { get, path } = store;

        const indexedItems: Map<string, { id: string; name: string; value: number }> = new Map();
        const items = get(path("items"));

        items.forEach(item => {
            indexedItems.set(item.id, { id: item.id, name: item.name, value: 0 });
        });

        orders.forEach(order => {
            order.items?.forEach(item => {
                if (item.colli && item.colli !== "0") {
                    if (indexedItems.has(item.id)) {
                        let previous = indexedItems.get(item.id)?.value;
                        if (typeof previous === "number") {
                            indexedItems.get(item.id)!.value = previous + 1;
                        }
                    } else {
                        indexedItems.set(item.id, { id: item.id, name: item.name, value: 1 });
                    }
                } else {
                    indexedItems.set(item.id, { id: item.id, name: item.name, value: 1 });
                }
            });
        });

        const data = [...indexedItems.values()];

        await setAnalytics(store)(data);
        CONTROL.setShowHourlassProcess(store)(false);

        return Promise.resolve();
    };

    return {
        data,
        count,
        columnConfig,
        loadData: (_number: number) => _loadData(_number, store)
    };
}

const Analytics = StoreContainer(AnalyticsWidget, "state", {
    getProperties,
    paths: path => [path("analytics")]
});

export default Analytics;
