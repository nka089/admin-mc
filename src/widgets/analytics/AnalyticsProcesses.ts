import { createCommand } from "src/processes/processUtils";
import { replace } from "@dojo/framework/stores/state/operations";
import { createProcess } from "@dojo/framework/stores/process";

export const setAnalytics = createProcess("set-analytics", [createCommand<any[]>(({ payload, path }) => [replace(path("analytics", "data"), payload)])]);

export const setAnalyticsCounter = createProcess("set-analytics-counter", [
    createCommand<Number>(({ path, payload }) => [replace(path("analytics", "counter"), Number(payload))])
]);
