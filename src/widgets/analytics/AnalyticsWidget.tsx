import Grid from "@dojo/widgets/grid";
import { ColumnConfig } from "@dojo/widgets/grid/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { DNode } from "@dojo/framework/core/interfaces";
import Button from "@dojo/widgets/button";
import TextInput from "@dojo/widgets/text-input";
import Icon from "@dojo/widgets/icon";
import { createFetcher } from "@dojo/widgets/grid/utils";
import { tsx } from "@dojo/framework/core/vdom";
import * as css from "./analytics.m.css";
// import diffProperty from "@dojo/framework/core/decorators/diffProperty";
// import { always } from "@dojo/framework/core/diff";

export interface AnalyticsApi {
    loadData: (_number: number) => Promise<void>;
    data: any[];
    columnConfig: ColumnConfig[];
    count: number;
}

export default class AnalyticsWidget extends WidgetBase<AnalyticsApi> {
    // @diffProperty("count", always)
    // protected onCounterChange(): void {
    //     this.invalidate();
    // }

    private numberOfOrder = 0;
    render(): DNode {
        const { loadData, columnConfig, data } = this.properties;

        return (
            <div classes={css.container}>
                <div classes={css.inputWrapper}>
                    <TextInput
                        label="Anzahl der Aufträge"
                        type="number"
                        onInput={(_number: number) => {
                            this.numberOfOrder = _number;
                        }}
                        onKeyUp={(key: number) => {
                            if (key === 13) {
                                loadData(this.numberOfOrder).then(() => {
                                    this.invalidate();
                                });
                            }
                        }}
                    ></TextInput>
                    <Button
                        extraClasses={{ root: css.button }}
                        onClick={() => {
                            loadData(this.numberOfOrder).then(() => {
                                this.invalidate();
                            });
                        }}
                    >
                        <Icon type="checkIcon" />
                    </Button>
                </div>
                <h4>Echte Anzahl der Aufträge: {String(this.properties.count)}</h4>
                <Grid pagination={true} key={Date.now()} height={500} columnConfig={columnConfig} fetcher={createFetcher(data)}></Grid>
            </div>
        );
    }
}
