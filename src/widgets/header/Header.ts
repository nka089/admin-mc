import { v } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { OutletPaths } from "../../routes";
import * as css from "./header.m.css";
import { version } from "src/version";
import { Utils } from "src/utils/Utils";

const HeaderBase = I18nMixin(WidgetBase);

export default class Header extends HeaderBase<{ to: (path: OutletPaths) => void }> {
    protected render(): DNode {
        const { to } = this.properties;
        let vrsn = Utils.replaceAll(version, ",", "");
        vrsn = Utils.replaceAll(vrsn, ":", "");

        vrsn = Utils.replaceAll(vrsn, "\\.", "");

        return v("div", { classes: [css.main] }, [
            v(
                "div",
                {
                    classes: [css.link],
                    onclick: () => {
                        to(OutletPaths.ORDERS);
                    }
                },
                ["Aufträge"]
            ),
            v(
                "div",
                {
                    classes: [css.link],

                    onclick: () => {
                        to(OutletPaths.SERVICES);
                    }
                },
                ["Leistungen"]
            ),

            v(
                "div",
                {
                    classes: [css.link],

                    onclick: () => {
                        to(OutletPaths.FURNITURE);
                    }
                },
                ["Mobiliar"]
            ),
            v(
                "div",
                {
                    classes: [css.link],

                    onclick: () => {
                        to(OutletPaths.OPTIONS);
                    }
                },
                ["Optionen"]
            ),
            v(
                "div",
                {
                    classes: [css.link],
                    onclick: () => {
                        to(OutletPaths.CATEGORIES);
                    }
                },
                ["Kategorien"]
            ),
            v(
                "div",
                {
                    classes: [css.link],
                    onclick: () => {
                        to(OutletPaths.SPECIAL);
                    }
                },
                ["Administration"]
            ),
            v("div", { classes: css.version }, [`${vrsn}`])
        ]);
    }
}
