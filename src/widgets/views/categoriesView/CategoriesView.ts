import { v, w } from "@dojo/framework/core/vdom";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { always } from "@dojo/framework/core/diff";
import { DNode } from "@dojo/framework/core/interfaces";
import { Category } from "../../../typings/types";
import Button from "@dojo/widgets/button";
import Input from "@dojo/widgets/text-input";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import * as css from "./categoriesView.m.css";
import H4 from "src/widgets/commons/h4/H4";

export interface CategoriesWidgetProperties {
    categories: Category[];
    createCategory: (name: string) => Promise<void>;
    deleteCategory: (id: string) => Promise<void>;
}

@diffProperty("categories", always)
export default class CategoriesView extends WidgetBase<CategoriesWidgetProperties> {
    private newCategory: string = "";

    protected render(): DNode {
        const { categories, deleteCategory } = this.properties;
        const input = w(Input, {
            value: this.newCategory,
            placeholder: "Neue Kategorie",
            onInput: (value: string) => {
                this.newCategory = value.trim();
                this.invalidate();
            }
        });
        const createButton = w(
            Button,
            {
                disabled: !this.newCategory,
                onClick: () => {
                    this.createCategory();
                }
            },
            ["Erstellen"]
        );
        const cats = categories.map(c => {
            return v("div", { classes: [css.row] }, [
                v("div", { classes: [css.catname] }, [c.name]),
                v("div", { classes: [css.button] }, [
                    w(
                        Button,
                        {
                            onClick: () => {
                                deleteCategory(c.id);
                            }
                        },
                        ["Entfernen"]
                    )
                ])
            ]);
        });
        const header = w(H4, { text: "Möbel Kategorien" });
        return v("div", { classes: [css.view] }, [header, v("div", cats), v("div", { classes: [css.row] }, [input, createButton])]);
    }

    private async createCategory(): Promise<void> {
        const { createCategory } = this.properties;
        await createCategory(this.newCategory);
        this.newCategory = "";
        this.invalidate();
    }
}
