import { v, w, tsx } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Button from "@dojo/widgets/button";
import * as css from "./ordersGridView.m.css";
import GridContainer, { editFunction } from "../../../containers/GridContainer";
import TextInput from "@dojo/widgets/text-input";
import H4 from "src/widgets/commons/h4/H4";
import Icon from "@dojo/widgets/icon";
import { Order, Service, Item, Address, TimeBasedPrice, Customer } from "src/typings/types";

export interface OrdersGridViewProperties {
    getOrdersByNameOrDate: (nameOrDate: string) => void;
    getLastOrders: (nmb: string) => void;
    getOrderById: (id: string) => void;
}

export default class OrdersGridView extends WidgetBase<OrdersGridViewProperties> {
    protected render(): DNode {
        const header = w(H4, { text: "Aufträge" });
        return v("div", { classes: [css.view] }, [w(CreateOrderWidget, {}), header, w(SearchWidget, this.properties), w(GridContainer, {})]);
    }
}

class CreateOrderWidget extends WidgetBase {
    protected render(): DNode {
        const services: Service[] = [];
        const items: Item[] = [];
        const order: Order = {
            customer: { company: "" } as Customer,
            from: { address: "" } as Address,
            to: { address: "" } as Address,
            timeBased: { basis: "" } as TimeBasedPrice,
            services,
            items,
            distance: 0
        } as Order;
        order.lupd = order.creationTime = new Date().toLocaleString();

        return (
            <div>
                <Button
                    onClick={() => {
                        editFunction(order);
                    }}
                >
                    <Icon type="plusIcon"></Icon>
                    Auftrag anlegen
                </Button>
            </div>
        );
    }
}

class SearchWidget extends WidgetBase<OrdersGridViewProperties> {
    private keyword = "";
    private last = "";
    private id = "";

    protected render(): DNode {
        const keywordInput = w(TextInput, {
            placeholder: "Suchbegriff",
            value: this.keyword,
            onInput: (value: any) => {
                this.keyword = value;
                this.invalidate();
            },
            onKeyUp: (key: number) => {
                if (key === 13) {
                    if (this.keyword) {
                        getOrdersByNameOrDate(this.keyword);
                    }
                }
            }
        });
        const idInput = w(TextInput, {
            value: this.id,
            placeholder: "ID",

            onKeyUp: (key: number) => {
                if (key === 13) {
                    if (this.id) {
                        getOrderById(this.id);
                    }
                }
            },

            onInput: (value: string) => {
                this.id = value;
            }
        });

        const { getOrdersByNameOrDate, getOrderById } = this.properties;
        const search = w(
            Button,
            {
                extraClasses: { root: css.button },
                onClick: () => {
                    if (this.keyword) {
                        getOrdersByNameOrDate(this.keyword);
                        this.invalidate();
                    }
                }
            },
            [
                w(Icon, {
                    type: "searchIcon"
                })
            ]
        );

        const idSearch = w(
            Button,
            {
                extraClasses: { root: css.button },
                onClick: () => {
                    if (this.id) {
                        getOrderById(this.id);
                    }
                    this.invalidate();
                }
            },
            [
                w(Icon, {
                    type: "searchIcon"
                })
            ]
        );

        const lastInput = w(TextInput, {
            value: this.last,
            placeholder: "Neueste Aufträge",
            onInput: (value: string) => {
                this.last = value;
            },
            onKeyUp: (key: number) => {
                if (key === 13) {
                    if (this.last) {
                        getLastOrders(this.last);
                    }
                }
            }
        });
        const { getLastOrders } = this.properties;

        const loadLast = w(
            Button,
            {
                extraClasses: { root: css.button },
                onClick: () => {
                    if (this.last) {
                        getLastOrders(this.last);
                    }
                }
            },
            [
                w(Icon, {
                    type: "searchIcon"
                })
            ]
        );
        const main = v("div", { classes: [css.search_main] }, [
            v("div", { classes: css.container }, [v("div", { classes: [css.name] }, [keywordInput]), v("div", { classes: [css.search] }, [search])]),
            v("div", { classes: css.container }, [v("div", { classes: [css.last] }, [lastInput]), v("div", { classes: [css.loadLast] }, [loadLast])]),
            v("div", { classes: css.container }, [v("div", { classes: [css.last] }, [idInput]), v("div", { classes: [css.loadLast] }, [idSearch])])
        ]);
        return main;
    }
}
