import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { Service } from "../../../typings/types";
import Input from "@dojo/widgets/text-input";
import Select from "@dojo/widgets/select";
import Button from "@dojo/widgets/button";
import { SERVICE_TAGS } from "../../../constants/Constants";
import * as css from "./servicesView.m.css";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { always } from "@dojo/framework/core/diff";
import { Utils } from "src/utils/Utils";
import TextArea from "@dojo/widgets/text-area";
import H4 from "src/widgets/commons/h4/H4";
import Icon from "@dojo/widgets/icon";

export interface ServicesViewProperties {
    services: Service[];
    createService: (service: Service) => Promise<void>;
    deleteService: (id: string) => Promise<void>;
    updateService: (item: Service) => Promise<void>;
}

const Base = I18nMixin(WidgetBase);

@diffProperty("services", always)
export default class ServicesView extends Base<ServicesViewProperties> {
    private name: string = "";
    private description: string = "";
    private price: string = "";
    private tag: string = SERVICE_TAGS()[0];
    url: string | undefined;

    protected render(): DNode {
        const { services, deleteService, updateService } = this.properties;

        const rows: DNode[] = services.map(service => w(ServiceWidget, { service, deleteService, updateService }));

        const lastrow = v("tr", [
            v("td", { classes: [css.cell] }, []),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    // extraClasses: { root: css.input },
                    value: this.name,
                    onInput: (value: string) => {
                        this.name = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(TextArea, {
                    value: this.description,
                    // extraClasses: { root: css.input },

                    onInput: (value: string) => {
                        this.description = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Select, {
                    value: this.tag,
                    options: SERVICE_TAGS(),
                    onChange: (value: string) => {
                        this.tag = value;
                        this.invalidate();
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    value: this.price,
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.price = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    value: this.url,
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.price = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell, css.createCell] }, [
                w(
                    Button,
                    {
                        extraClasses: { root: css.newButton },
                        onClick: () => {
                            this.createService();
                        }
                    },
                    ["Neu"]
                )
            ])
        ]);
        rows.push(lastrow);

        const servicesTable = v("table", { classes: [css.table] }, [
            v("thead", [
                v("th", { classes: [css.cell] }, ["id"]),
                v("th", { classes: [css.cell] }, ["Name"]),
                v("th", { classes: [css.cell] }, ["Beschreibung"]),
                v("th", { classes: [css.cell, css.tagCell] }, ["Tag"]),
                v("th", { classes: [css.cell, css.priceCell] }, ["Price"]),
                v("th", { classes: [css.cell, css.urlCell] }, ["Bild URL"]),
                v("th", { classes: [css.cell, css.btnsCell] }, ["Editieren"])
            ]),
            v("tbody", rows)
        ]);
        const header = w(H4, { text: "Leistungen" });
        return v("div", { classes: [css.view] }, [header, v("div", [servicesTable])]);
    }

    private async createService(): Promise<void> {
        const { createService } = this.properties;
        const newService = <Service>{};
        newService.price = this.price;
        newService.tag = this.tag;
        newService.name = this.name;
        newService.description = this.description;
        this.name = this.price = this.description = "";

        await createService(newService);
        this.invalidate();
    }
}

interface ServiceWidgetProps {
    service: Service;
    deleteService: (id: string) => Promise<void>;
    updateService: (service: Service) => Promise<void>;
}

class ServiceWidget extends Base<ServiceWidgetProps> {
    private service: Service = <Service>{ tag: SERVICE_TAGS()[0], price: "0" };
    protected render(): DNode {
        let row = null;

        const { service, deleteService, updateService } = this.properties;
        this.service = service;
        row = v("tr", [
            v("td", { classes: [css.cell] }, [`${service.id}`]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    value: this.service.name,
                    onInput: (value: any) => {
                        this.service.name = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(TextArea, {
                    // extraClasses: { root: css.input },
                    value: this.service.description,
                    onInput: (value: string) => {
                        this.service.description = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [service.tag]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    value: this.service.price,

                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.service.price = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    value: this.service.imgUrl,

                    onInput: (value: any) => {
                        const input = value.trim();

                        this.service.imgUrl = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                v("div", { classes: css.buttons }, [
                    w(
                        Button,
                        {
                            extraClasses: { root: css.button },
                            onClick: async () => {
                                await updateService(this.service);
                                this.invalidate();
                            }
                        },
                        [
                            w(Icon, {
                                type: "checkIcon"
                            })
                        ]
                    ),
                    w(
                        Button,
                        {
                            extraClasses: { root: css.deleteButton },
                            onClick: async () => {
                                await deleteService(service.id);
                                this.invalidate();
                            }
                        },
                        [
                            w(Icon, {
                                type: "closeIcon"
                            })
                        ]
                    )
                ])
            ])
        ]);

        return row;
    }
}
