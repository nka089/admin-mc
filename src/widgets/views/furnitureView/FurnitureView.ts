import { v, w } from "@dojo/framework/core/vdom";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { always } from "@dojo/framework/core/diff";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import Button from "@dojo/widgets/button";
import Checkbox from "@dojo/widgets/checkbox";
import Icon from "@dojo/widgets/icon";
import Input from "@dojo/widgets/text-input";
import { Utils } from "src/utils/Utils";
import H4 from "src/widgets/commons/h4/H4";
import { Category, Item } from "../../../typings/types";
import * as css from "./furnitureView.m.css";

const Base = I18nMixin(WidgetBase);

export interface FurnitureViewProperties {
    items: Item[];
    categories: Category[];
    createItem: (item: Item) => Promise<void>;
    updateItem: (item: Item) => Promise<void>;
    deleteItem: (id: string) => Promise<void>;
}

export default class FurnitureView extends Base<FurnitureViewProperties> {
    protected render(): DNode {
        const header = w(H4, { text: "Möbellisten" });
        const body = v("div", {}, [w(ItemsWidget, this.properties)]);
        return v("div", { classes: [css.view] }, [header, body]);
    }
}

@diffProperty("items", always)
class ItemsWidget extends Base<FurnitureViewProperties> {
    private catIds: Category[] = [];
    private item: Item = <Item>{ extraPrice: "0", volume: 0, montagePrice: "0" };

    private createCategories(): DNode {
        const { categories } = this.properties;

        const checkboxes = categories.map(c => {
            const cb = w(Checkbox, {
                label: c.name,
                onChange: (value: string, checked: boolean) => {
                    if (checked === true) {
                        this.catIds.push(c);
                    } else {
                        this.catIds = this.catIds.filter(cat => cat.id !== c.id);
                    }
                    this.invalidate();
                }
            });

            return v("div", { classes: [css.checkBoxDiv] }, [cb]);
        });
        return v("div", { classes: [css.flexBox] }, checkboxes);
    }

    private createProperties(): DNode {
        return v("div", { classes: css.flexBox }, [
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Demontage",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.demontage = false;
                        } else {
                            delete this.item.demontage;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Montage",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.montage = false;
                        } else {
                            delete this.item.montage;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Mehr 100",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.m100 = false;
                        } else {
                            delete this.item.m100;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Mehr 150",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.m150 = false;
                        } else {
                            delete this.item.m150;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Nicht zerlegbar",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.notDismountable = false;
                        } else {
                            delete this.item.notDismountable;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    label: "Sperrig",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.bulky = false;
                        } else {
                            delete this.item.bulky;
                        }
                        this.invalidate();
                    }
                })
            ])
        ]);
    }

    protected render(): DNode {
        return v("div", { classes: [css.widget] }, [v("div", {}, [this.createItemsTable()])]);
    }

    private createItemsTable(): DNode {
        const { items } = this.properties;
        items.sort((i1, i2) => {
            if (i1.name < i2.name) {
                return -1;
            }
            if (i1.name > i2.name) {
                return 1;
            }
            return 0;
        });
        const bodyArr: DNode[] = items.map(item =>
            w(ItemRowWdiget, { item, categories: this.properties.categories, deleteById: this.properties.deleteItem, update: this.properties.updateItem })
        );

        const lastrow = v("tr", { key: "newItem" + Date.now() }, [
            v("td", { classes: [css.cell] }, []),
            v("td", { classes: [css.cell, css.inputCell] }, [
                w(Input, {
                    placeholder: "Name",
                    onInput: (value: string) => {
                        this.item.name = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell, css.smallInputCell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    placeholder: "Volume",
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.volume = parseFloat(input);
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [this.createProperties()]),
            v("td", { classes: [css.cell, css.catsCell] }, [this.createCategories()]),
            v("td", { classes: [css.cell, css.smallInputCell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    placeholder: "Extrakosten",

                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.extraPrice = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    placeholder: "Montagepreis",

                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.montagePrice = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell, css.buttonsCell] }, [
                w(
                    Button,
                    {
                        extraClasses: { root: css.newButton },
                        onClick: () => {
                            this.createItem();
                        }
                    },
                    ["Neu"]
                )
            ])
        ]);

        bodyArr.push(lastrow);
        const table = v("table", { classes: [css.table] }, [
            v("thead", [
                v("th", { classes: [css.cell, css.th] }, ["ID"]),
                v("th", { classes: [css.cell, css.th, css.name] }, ["Name"]),
                v("th", { classes: [css.cell, css.th] }, ["Volume"]),
                v("th", { classes: [css.cell, css.th] }, ["Eigenschaften"]),
                v("th", { classes: [css.cell, css.th] }, ["Kategorien"]),
                v("th", { classes: [css.cell, css.th] }, ["Extrakosten"]),
                v("th", { classes: [css.cell, css.th] }, ["Montagepreis"]),
                v("th", { classes: [css.cell, css.th, css.buttons] }, ["Operationen"])
            ]),
            v("tbody", bodyArr)
        ]);
        return table;
    }

    private async createItem(): Promise<void> {
        const { createItem } = this.properties;
        this.item.categoryRefs = this.catIds;
        if (this.item.categoryRefs.length === 0) {
            alert("Keine Kategorie ausgewählt!");
            return;
        }
        if (this.item.volume === 0 && this.item.extraPrice === "0") {
            alert("entweder Volumen oder Extrakosten müssen gesetzt sein!");
            return;
        }
        await createItem(this.item);
        this.item = <Item>{ extraPrice: "", volume: 0 };
        this.catIds = [];
        this.invalidate();
    }
}

interface ItemRowWdigetProperties {
    categories: Category[];
    item: Item;
    deleteById: (id: string) => Promise<void>;
    update: (item: Item) => Promise<void>;
}
class ItemRowWdiget extends WidgetBase<ItemRowWdigetProperties> {
    private showRed = false;
    private catIds: Category[] = [];
    private item: Item = <Item>{ extraPrice: "", volume: 0 };

    protected render(): DNode {
        const { item } = this.properties;
        this.item = item;

        return v("tr", { key: "newItem" + Date.now(), classes: this.showRed ? css.red : null }, [
            v("td", { classes: [css.cell] }, [`${this.item.id}`]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    value: this.item.name,
                    onInput: (value: string) => {
                        this.item.name = value;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    value: this.item.volume.toString(),
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.volume = parseFloat(input);
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [this.createProperties()]),
            v("td", { classes: [css.cell] }, [this.createCategories()]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    extraClasses: { root: css.input },
                    placeholder: "Extrakosten",
                    value: this.item.extraPrice ? this.item.extraPrice : "0",
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.extraPrice = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                w(Input, {
                    placeholder: "Montagepreis",

                    extraClasses: { root: css.input },
                    value: this.item.montagePrice ? this.item.montagePrice : "0",
                    onInput: (value: string) => {
                        let input = value.trim();
                        input = Utils.replaceAll(input, ",", ".");
                        this.item.montagePrice = input;
                    }
                })
            ]),
            v("td", { classes: [css.cell] }, [
                v("div", { classes: css.ops }, [
                    w(
                        Button,
                        {
                            extraClasses: { root: css.button },
                            onClick: () => this.update()
                        },
                        [w(Icon, { type: "checkIcon" })]
                    ),
                    w(
                        Button,
                        {
                            extraClasses: { root: css.deleteButton },
                            onClick: () => this.deleteById()
                        },
                        [w(Icon, { type: "closeIcon" })]
                    )
                ])
            ])
        ]);
    }

    private createProperties(): DNode {
        return v("div", { classes: css.flexBox }, [
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.demontage === "boolean",
                    label: "Demontage",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.demontage = false;
                        } else {
                            delete this.item.demontage;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.montage === "boolean",
                    label: "Montage",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.montage = false;
                        } else {
                            delete this.item.montage;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.m100 === "boolean",
                    label: "Mehr 100",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.m100 = false;
                        } else {
                            delete this.item.m100;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.m150 === "boolean",
                    label: "Mehr 150",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.m150 = false;
                        } else {
                            delete this.item.m150;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.notDismountable === "boolean",
                    label: "Nicht zerlegbar",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.notDismountable = false;
                        } else {
                            delete this.item.notDismountable;
                        }
                        this.invalidate();
                    }
                })
            ]),
            v("div", { classes: css.checkBoxDiv }, [
                w(Checkbox, {
                    checked: typeof this.item.bulky === "boolean",
                    label: "Sperrig",
                    onChange: (value: string, checked: boolean) => {
                        if (checked) {
                            this.item.bulky = false;
                        } else {
                            delete this.item.bulky;
                        }
                        this.invalidate();
                    }
                })
            ])
        ]);
    }

    private createCategories(): DNode {
        const { categories } = this.properties;

        const checkboxes = categories.map(c => {
            let checked = false;
            if (this.item.categoryRefs && this.item.categoryRefs.findIndex(cat => cat.id === c.id) > -1) {
                checked = true;
                this.catIds.push(c);
            }
            const cb = w(Checkbox, {
                checked,
                label: c.name,
                onChange: (value: string, checked: boolean) => {
                    if (checked === true) {
                        this.catIds.push(c);
                    } else {
                        this.catIds = this.catIds.filter(cat => cat.id !== c.id);
                    }
                    this.item.categoryRefs = this.catIds;
                    this.invalidate();
                }
            });

            return v("div", { classes: [css.checkBoxDiv] }, [cb]);
        });
        return v("div", { classes: [css.flexBox] }, checkboxes);
    }

    private async deleteById(): Promise<void> {
        const { deleteById } = this.properties;
        await deleteById(this.item.id);
        this.invalidate();
    }

    private async update(): Promise<void> {
        const { update } = this.properties;
        this.item.categoryRefs = this.catIds;
        if (this.item.categoryRefs.length === 0) {
            alert("Keine Kategorie ausgewählt!");
            this.showRed = true;
            this.invalidate();
            return;
        }
        if (this.item.volume === 0 && this.item.extraPrice === "0") {
            alert("Entweder Volumen oder Extrakosten müssen gesetzt sein!");
            this.showRed = true;
            this.invalidate();
            return;
        }
        await update(this.item);
        this.showRed = false;
        this.invalidate();
    }
}
