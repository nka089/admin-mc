import WidgetBase from "@dojo/framework/core/WidgetBase";
import { w, v } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import PriceCalculatorContainer from "src/containers/PriceCalculatorContainer";
import * as css from "./orderEditorView.m.css";
import OrderEditorContainer from "src/containers/OrderEditorContainer";

export default class OrderEditorView extends WidgetBase {
    protected render(): DNode {
        const view = v("div", { classes: [css.main] }, [
            v("div", { classes: css.editor }, [w(OrderEditorContainer, {})]),
            v("div", { classes: css.calc }, [w(PriceCalculatorContainer, {})])
        ]);
        return view;
    }
}
