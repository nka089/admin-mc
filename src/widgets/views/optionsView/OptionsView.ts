import WidgetBase from "@dojo/framework/core/WidgetBase";
import { v, w } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import Input from "@dojo/widgets/text-input";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { always } from "@dojo/framework/core/diff";
import Button from "@dojo/widgets/button";
import Map from "@dojo/framework/shim/Map";
import * as css from "./optionsView.m.css";
import translations from "../../../nls/translations";
import { Utils } from "src/utils/Utils";
import H4 from "src/widgets/commons/h4/H4";
import TabController from "@dojo/widgets/tab-controller";
import Tab from "@dojo/widgets/tab";
import Icon from "@dojo/widgets/icon";
import { uuid } from "@dojo/framework/core/util";

export interface OptionsViewProperties {
    options: { [key: string]: string };
    updateOption: (key: string, value: string) => void;
}

const Base = I18nMixin(WidgetBase);
@diffProperty("options", always)
export default class OptionsView extends Base<OptionsViewProperties> {
    private map: Map<string, string> = new Map();
    private tabIndex: number = 0;

    protected render(): DNode {
        const { messages } = this.localizeBundle(translations);
        const { options: _opts, updateOption } = this.properties;

        const header = w(H4, { text: "Optionen" });
        const opts: DNode[] = [];
        const a_opts: DNode[] = [];
        const s_opts: DNode[] = [];

        const oNames = Object.getOwnPropertyNames(_opts);

        const options: { key: string; value: string }[] = [];
        oNames.forEach(o => {
            options.push({ key: o, value: _opts[o] });
        });

        options
            .filter(o => {
                //@ts-ignore
                let desc = messages[o.key];
                return !desc?.startsWith("AGB") && !desc?.startsWith("SERV");
            })
            .forEach(o => {
                this.map.set(o.key, o.value);

                const oDiv = w(OptionsDiv, {
                    o,
                    setMap: (key: string, value: string) => this.map.set(key, value),
                    updateOption: (key: string) => {
                        updateOption(key, this.map.get(o.key) || "");
                    }
                });
                opts.push(oDiv);
            });

        options
            .filter(o => {
                //@ts-ignore
                const desc = messages[o.key];
                return desc?.startsWith("AGB") && !desc?.startsWith("SERV");
            })
            .forEach(o => {
                this.map.set(o.key, o.value);
                const oDiv = w(OptionsDiv, {
                    o,
                    setMap: (key: string, value: string) => this.map.set(key, value),
                    updateOption: (key: string) => {
                        updateOption(key, this.map.get(o.key) || "");
                    }
                });

                a_opts.push(oDiv);
            });

        options
            .filter(o => {
                //@ts-ignore
                const desc = messages[o.key];
                return !desc?.startsWith("AGB") && desc?.startsWith("SERV");
            })
            .forEach(o => {
                this.map.set(o.key, o.value);
                const oDiv = w(OptionsDiv, {
                    o,
                    setMap: (key: string, value: string) => this.map.set(key, value),
                    updateOption: (key: string) => {
                        updateOption(key, this.map.get(o.key) || "");
                    }
                });

                s_opts.push(oDiv);
            });

        return v("div", { classes: [css.view] }, [
            header,
            w(
                TabController,
                {
                    activeIndex: this.tabIndex,
                    onRequestTabChange: (index: number) => {
                        this.tabIndex = index;
                        this.invalidate();
                    }
                },
                [
                    w(
                        Tab,
                        {
                            key: "opts",
                            label: "Rechner Optionen"
                        },
                        opts
                    ),
                    w(
                        Tab,
                        {
                            key: "agbOpts",
                            label: "AGB Optionen"
                        },
                        a_opts
                    ),
                    w(
                        Tab,
                        {
                            key: "servOpts",
                            label: "E-Mail Server Optionen"
                        },
                        s_opts
                    )
                ]
            )
        ]);
    }
}

interface OptionsDivProps {
    o: { key: string; value: string };
    setMap: (key: string, value: string) => void;
    updateOption: (key: string) => void;
}

class OptionsDiv extends Base<OptionsDivProps> {
    protected render(): DNode {
        const { messages } = this.localizeBundle(translations);

        const { o, setMap, updateOption } = this.properties;

        return v("div", { key: uuid(), classes: [css.optContainer] }, [
            v("div", { classes: css.flex }, [
                v(
                    "div",
                    {
                        classes: [css.input]
                    },
                    [
                        w(Input, {
                            //@ts-ignore
                            label: messages[o.key] || o.key,
                            key: o.key,
                            value: o.value,
                            onInput: (value: string) => {
                                let input = value.trim();
                                input = Utils.replaceAll(input, ",", ".");
                                setMap(o.key, input);
                            }
                        })
                    ]
                ),
                w(
                    Button,
                    {
                        extraClasses: {
                            root: css.button
                        },
                        onClick: () => {
                            updateOption(o.key);
                        }
                    },
                    [w(Icon, { type: "checkIcon" })]
                )
            ])
        ]);
    }
}
