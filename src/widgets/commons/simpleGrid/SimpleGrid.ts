import WidgetBase from "@dojo/framework/core/WidgetBase";
import { SimpleGridProperties, ColumnConfig } from "./interfaces";
import { DNode, SupportedClassName } from "@dojo/framework/core/interfaces";
import { v, w } from "@dojo/framework/core/vdom";
import * as css from "./simpleGrid.m.css";
import diffProperty from "@dojo/framework/core/decorators/diffProperty";
import { always } from "@dojo/framework/core/diff";
import Radio from "@dojo/widgets/radio";

export default class SimpleGrid extends WidgetBase<SimpleGridProperties> {
    private radioValue: "a" | "y" | "n" = "a";
    private readonly cellHeight = 35;
    private data: any[] = [];
    private desc = false;
    private entriesProPage: number = 1;
    private pages: number | undefined = undefined;

    private selectedPageNumber = 0;
    private range: { from: number; to: number } | undefined = undefined;

    protected render(): DNode {
        const { data } = this.properties;
        this.calculateEntriesProPage();

        if (this.radioValue === "a") {
            this.data = data;
        } else if (this.radioValue === "y") {
            const _value = this.properties.columnConfig.find(c => c.filterable === true);
            if (_value) {
                this.data = data.filter(d => d[_value.id] === true);
            }
        } else {
            const _value = this.properties.columnConfig.find(c => c.filterable === true);
            if (_value) {
                this.data = data.filter(d => d[_value.id] === false || typeof d[_value.id] === "undefined");
            }
        }
        if (this.data.length > this.entriesProPage) {
            if (!this.range) {
                this.range = { from: 0, to: this.entriesProPage };
            }
            this.pages = Math.ceil(this.data.length / this.entriesProPage);
        }
        const header = this.renderHeader();
        const body = this.renderBody();
        const footer = this.renderFooter();
        const table = v("div", { classes: css.table }, [header, body]);
        return v("div", { classes: css.root }, [table, footer]);
    }

    private calculateEntriesProPage(): void {
        this.entriesProPage = Math.round((this.properties.height - 4 * this.cellHeight) / this.cellHeight);
    }

    private renderFooter(): DNode {
        const footer: DNode[] = [];
        if (this.pages) {
            for (let i = 0; i < this.pages; i++) {
                let j = i + 1;
                footer.push(
                    v(
                        "div",
                        {
                            classes: [css.pageNumber, this.selectedPageNumber === j ? css.pageNumberSelected : null],
                            onclick: () => {
                                this.selectedPageNumber = j;
                                let from = i * this.entriesProPage;
                                let to = j * this.entriesProPage;
                                this.range = { from, to };
                                this.invalidate();
                            }
                        },
                        [j.toString()]
                    )
                );
            }
        }
        return v("div", { classes: css.footer }, footer);
    }

    private renderHeader(): DNode {
        const { columnConfig } = this.properties;

        const headerRow = columnConfig.map(config => {
            const classes: SupportedClassName[] = [];
            let title = v("th", { classes }, [`${config.title}${config.sortable === true ? " ⇵" : ""}`]);
            if (config.sortable) {
                classes.push(css.sortableHeader);
                title.properties.onclick = () => {
                    this.desc = !this.desc;
                    this.data.sort((a, b) => {
                        return this.sort(a[config.id], b[config.id]);
                    });
                    this.invalidate();
                };
            }
            if (config.filterable) {
                title = v("div", { classes: css.cell }, [
                    v("div", { classes: css.radioTitle }, [config.title]),

                    v("div", { classes: css.radios }, [
                        w(Radio, {
                            checked: this.radioValue === "a",
                            // describedBy: "filter",
                            label: "Alle",
                            name: "radio1",
                            value: "a",
                            onChange: (event: any) => {
                                this.radioValue = "a";
                                this.data = this.properties.data;
                                this.invalidate();
                            }
                        }),
                        w(Radio, {
                            checked: this.radioValue === "y",
                            // describedBy: "filter",
                            label: "Ja",
                            name: "radio1",
                            value: "y",
                            onChange: (event: any) => {
                                this.radioValue = "y";
                                this.data = this.data.filter(d => d[config.id] === true);
                                this.invalidate();
                            }
                        }),
                        w(Radio, {
                            checked: this.radioValue === "n",
                            // describedBy: "filter",
                            label: "Nein",
                            name: "radio1",
                            value: "n",
                            onChange: (event: any) => {
                                console.log("Nein");
                                this.radioValue = "n";
                                this.data = this.data.filter(d => d[config.id] === false);
                                this.invalidate();
                            }
                        })
                    ])
                ]);
            }
            classes.push(css.cell);
            return title;
        });

        return v("div", { classes: css.header }, headerRow);
    }

    private sort(a: any, b: any): number {
        if (typeof a === "number" && typeof b === "number") {
            if (this.desc) {
                return a - b;
            } else {
                return b - a;
            }
        } else if (typeof a === "string" && a.length === 10 && a.includes(".")) {
            a = a
                .split(".")
                .reverse()
                .join("-");
            b = b
                .split(".")
                .reverse()
                .join("-");
            const dateA = new Date(a);
            const dateb = new Date(b);
            if (this.desc) {
                return dateA.getTime() - dateb.getTime();
            } else {
                return dateb.getTime() - dateA.getTime();
            }
        } else if (typeof a === "string" && typeof b === "string") {
            if (this.desc) {
                return a.localeCompare(b);
            } else {
                return b.localeCompare(a);
            }
        } else {
            return 0;
        }
    }

    private renderBody(): DNode {
        let _data = this.data;
        if (this.range) {
            _data = this.data.slice(this.range.from, this.range.to);
        }
        return w(SimpleGridBody, { data: _data, colConfig: this.properties.columnConfig });
    }
}

@diffProperty("data", always)
class SimpleGridBody extends WidgetBase<{ data: any[]; colConfig: ColumnConfig[] }> {
    private hell = false;
    protected render(): DNode {
        if (this.properties.data) {
            const rows = this.properties.data.map(entry => this.renderRow(entry));
            return v("div", { classes: css.body }, rows);
        } else {
            return null;
        }
    }

    private renderRow(entry: any): DNode {
        this.hell = !this.hell;
        const { colConfig } = this.properties;

        const cells = colConfig.map((c: ColumnConfig) => {
            if (c.edit) {
                return v("div", { classes: css.cell }, [
                    v(
                        "button",
                        {
                            onclick: () => {
                                //@ts-ignore
                                c.edit.function(entry);
                            }
                        },
                        [c.edit.text]
                    )
                ]);
            }
            let value = entry;
            if (c.childKeys) {
                for (let key of c.childKeys) {
                    value = value && value[key];
                }
            } else {
                value = entry[c.id];
            }
            if (c.flag) {
                return v("div", { key: Date.now(), classes: [css.cell, value ? css.signalred : null] }, [value ? "Ja" : "Nein"]);
            }
            return v("div", { key: Date.now(), classes: css.cell }, [value ? value.toString() : ""]);
        });
        return v("div", { classes: [css.row, this.hell ? null : css.greyRow] }, cells);
    }
}
