export interface SimpleGridProperties {
    data: any[];
    columnConfig: ColumnConfig[];
    height: number;
}

export interface ColumnConfig {
    id: string;
    title: string;
    sortable?: boolean;
    filterable?: boolean;
    flag?: boolean;
    edit?: {function: Function; text: string};
    childKeys?: string[];
}
