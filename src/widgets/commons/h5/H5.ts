import { v } from "@dojo/framework/core/vdom";
import { DNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import * as css from "./h5.m.css";

export interface H5Properties {
    text: string;
}

export default class H5 extends WidgetBase<H5Properties> {
    protected render(): DNode {
        const { text } = this.properties;
        return v("div", { classes: [css.root] }, [text]);
    }
}
