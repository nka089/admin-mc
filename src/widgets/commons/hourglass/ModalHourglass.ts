import { v, w } from "@dojo/framework/core/vdom";
import { VNode } from "@dojo/framework/core/interfaces";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import * as mcss from "./hourglass.m.css";

export interface ModalHourGlassProperties {
    show: boolean;
}

export default class ModalHourglass extends WidgetBase<ModalHourGlassProperties> {
    protected render(): VNode {
        const { show } = this.properties;

        return v("div", { classes: [mcss.modal, show ? mcss.show : mcss.hide] }, [w(Hourglass, { show })]);
    }
}

class Hourglass extends WidgetBase<ModalHourGlassProperties> {
    protected render(): VNode {
        const { show } = this.properties;

        return v("div", { classes: [mcss.loaderBackground, show ? mcss.show : mcss.hide] }, [
            v("div", { class: mcss.loader })
        ]);
    }
}
