export const show: string;
export const hide: string;
export const modal: string;
export const loader: string;
export const spin: string;
export const loaderBackground: string;
