import { v } from "@dojo/framework/core/vdom";
import { VNode } from "@dojo/framework/core/interfaces";
import ThemedMixin, { theme } from "@dojo/framework/core/mixins/Themed";
import { WidgetBase } from "@dojo/framework/core/WidgetBase";
import * as css from "./h4.m.css";

export interface H4Properties {
    text: string;
}

const H4Base = ThemedMixin(WidgetBase);

@theme(css)
export default class H4 extends H4Base<H4Properties> {
    protected render(): VNode {
        const { text } = this.properties;

        return v("div", { classes: [this.theme(css.root)] }, [text]);
    }
}
