import { Order } from "../../typings/types";
import I18nMixin from "@dojo/framework/core/mixins/I18n";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { DNode } from "@dojo/framework/core/interfaces";
import { w, v } from "@dojo/framework/core/vdom";

import SimpleGrid from "../commons/simpleGrid/SimpleGrid";
import { ColumnConfig } from "../commons/simpleGrid/interfaces";

export interface OrdersGridproperties {
    orders: Order[];
    columnConfig: ColumnConfig[];
}

const Base = I18nMixin(WidgetBase);
export default class OrdersGrid extends Base<OrdersGridproperties> {
    protected render(): DNode {
        const { orders, columnConfig } = this.properties;

        return orders && orders.length > 0
            ? w(SimpleGrid, { key: Date.now(), columnConfig, data: orders, height: window.innerHeight - 250 })
            : v("div", ["keine Aufträge geladen"]);
    }
}
