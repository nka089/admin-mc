import { _store } from "src/main";
import { OPTIONS } from "src/constants/Constants";
import { MasterData } from "src/processes/MasterDataProceses";
import { Order } from "src/typings/types";

export namespace Calculations {
    /**
     * g1  - Stockwerk Auszugadresse
     * g2  - Stockwerk Einzugsadresse
     * p    - Koeffizient Lift <= 4
     * q    - Koeffizient Lift > 4
     * f    - Fahrtkosten
     * h    - Halteverbotszonen
     * e    - Extra Kosten für Sondermöbel(Klavier, etc.)
     * s    - Services (Bohrarbeiten, Packmaterial)
     * v    - Umzugsgutvolumenkosten
     * b    - Basispreis
     *
     * Berechnung:
     *
     * Festkosten(F) = f + h + e + s;
     *
     * { k1, k2 } -> { k* =  (g* > 4) ? (g* * q) : (g* * p) }
     *
     * t    - KOeffizient für die Distanz zum Laufen
     * Koeffizient-Aufpreis für Stockwerke(K) = k1 + k2 + t
     *
     * Variable Kosten(V) = v + b
     *
     *
     * Summe(E) = F + V * K
     * evtl. discount -> Rabatt in % oder delta
     *
     */
    export const calculateSum = async (order: Order): Promise<Order> => {
        console.log("calculations call");
        order.prices = {};
        calculateVolumeAndVolumePrice(order);
        calculateRideCosts(order);

        const ogA = order.from.floor;
        const ogE = order.to.floor;

        const liftA = order.from.liftType;
        const LiftE = order.to.liftType;

        const distA = order.from.runningDistance;
        const distE = order.to.runningDistance;
        const T = calcTragewege(distA, distE);

        const k1: number = await getKoeff(ogA, liftA);

        const k2: number = await getKoeff(ogE, LiftE);

        const K: number = (k1 + k2 + T + 100) / 100;

        const f: string = order.rideCosts;
        const h: number = calcHVZ(order);
        order.prices.halteverbotszonen = h;

        const s = calcServicesPrice(order);
        order.prices.services = s;
        const v: string = order.volumePrice;
        const b: string = MasterData.getOption(OPTIONS.BASE_PRICE);

        const F = parseFloat(f) + h + s;
        const V = parseFloat(v) + parseFloat(b);

        let E: number = F + K * V;
        const loftPrice = parseFloat(MasterData.getOption(OPTIONS.LOFT_PRICE)) || 0;
        if (order.from.hasLoft === true) {
            E = E + loftPrice;
        }
        if (order.to.hasLoft === true) {
            E = E + loftPrice;
        }
        const discount = order.discount;
        if (discount) {
            if (discount.includes("%")) {
                //percent as string
                const p_s = discount.substr(0, discount.length - 1);
                // percent as float
                const p_f = parseFloat(p_s);
                if (typeof p_f === "number") {
                    E = (E * (100 + p_f)) / 100;
                }
            } else {
                let delta = parseFloat(discount);
                if (typeof delta === "number") {
                    E = E + delta;
                }
            }
        }
        order.sum = E.toFixed(2);
        return order;
    };
}
const LKW_VOL_SIZE = 20;

const calculateRideCosts = (order: Order): void => {
    const freeKmStr = MasterData.getOption(OPTIONS.BASE_KM);
    const freeKm = parseFloat(freeKmStr);
    let km = order.distance;

    let K = Math.ceil(parseFloat(order.volume) / LKW_VOL_SIZE);
    K = K > 0 ? K : 1;

    if (km - freeKm > 0) {
        km = km - freeKm;
        const price = MasterData.getOption(OPTIONS.KM_PRICE);
        const sum = parseFloat(price) * km * K;
        order.rideCosts = sum.toFixed(2);
    } else {
        order.rideCosts = "0";
    }
};

const calculateVolumeAndVolumePrice = (order: Order): void => {
    let volume = 0;
    let price = 0;

    const boxNumber = order.boxNumber;

    const boxVolume = MasterData.getOption(OPTIONS.BOX_CBM);
    if (typeof boxNumber === "number" && boxVolume && boxNumber > 0) {
        volume = boxNumber * parseFloat(boxVolume);
    }
    const extraCBM: number = order.extraCbm ? parseFloat(order.extraCbm) : 0;
    volume += extraCBM;
    order.items
        .filter(f => f.colli)
        .forEach(f => {
            const vol = parseInt(f.colli) * f.volume;

            volume += vol;
        });

    order.volume = volume.toFixed(2);

    const freeVolumeStr = MasterData.getOption(OPTIONS.BASE_QM);
    const freeVolume = parseFloat(freeVolumeStr);

    let K = Math.ceil(volume / LKW_VOL_SIZE) - 1;
    if (volume > freeVolume) {
        let paidvolume = volume - freeVolume;
        const volumePrice = MasterData.getOption(OPTIONS.QM_PRICE);
        price = paidvolume * parseFloat(volumePrice);
    }

    K = K > 0 ? K : 0;
    // calc every lkw
    let lkwPreis = parseFloat(MasterData.getOption(OPTIONS.LKW_PRICE)) * K;
    price = price + lkwPreis;
    const extraFurniture = calculateExtraFurniture(order);
    price = price + extraFurniture;

    order.volumePrice = price.toFixed(2);
};

const calcTragewege = (distA: string, distE: string): number => {
    let coef: number = 0;

    const t1 = distA?.split(" ")[0] || "0";
    const t2 = distE?.split(" ")[0] || "0";

    const v1 = parseFloat(t1);
    const v2 = parseFloat(t2);

    const extra = MasterData.getOption(OPTIONS.CHARGE_LFM);
    const baseLfm = MasterData.getOption(OPTIONS.BASE_LFM);
    let v = v1 + v2 - parseFloat(baseLfm);
    v = v < 0 ? 0 : v;
    coef = (v / 10) * parseFloat(extra); // every ten meters
    return coef;
};

//services Leistungen
const calcServicesPrice = (order: Order): number => {
    let sum = 0;

    const services = order.services;
    services
        .filter(s => s.colli)
        .forEach(s => {
            sum += parseFloat(s.price) * parseInt(s.colli);
        });
    return sum;
};

//parking slot - halteverbotszone
const calcHVZ = (order: Order): number => {
    let sum = 0;
    const a = order.from.parkingSlot;
    const b = order.to.parkingSlot;
    const price = MasterData.getOption(OPTIONS.HVZ_PRICE);
    if (a) {
        sum = sum + parseFloat(price);
    }
    if (b) {
        sum = sum + parseFloat(price);
    }
    return sum;
};

const getKoeff = async (og: string, lift: string): Promise<number> => {
    if (og === "EG") {
        return 0;
    } else {
        let g = parseInt(og?.substr(0, 1) || "0");
        let l = parseInt(lift?.substring(0, 1) || "0");
        if (l && l > 4) {
            let p = MasterData.getOption(OPTIONS.CHARGE_ETAGELIFT);
            return parseFloat(p) * g;
        } else {
            let q = MasterData.getOption(OPTIONS.CHARGE_ETAGE04);
            return parseFloat(q) * g;
        }
    }
};

const calculateExtraFurniture = (order: Order): number => {
    let furnitureExtra = 0;

    order.items
        .filter(f => f.colli)
        .forEach(f => {
            if (f.extraPrice) {
                let t0 = parseFloat(f.extraPrice) * parseInt(f.colli);
                furnitureExtra += t0;
            }
            if (f.montagePrice) {
                let t0 = parseFloat(f.montagePrice) * parseInt(f.colli);
                if (f.demontage === true) {
                    furnitureExtra += t0;
                }
                if (f.montage === true) {
                    furnitureExtra += t0;
                }
            }
            if (f.m100 && !f.extraPrice) {
                let t1 = parseFloat(MasterData.getOption(OPTIONS.M100_PRICE)) * parseInt(f.colli);
                furnitureExtra += t1;
            }
            if (f.m150 && !f.extraPrice) {
                let t2 = parseFloat(MasterData.getOption(OPTIONS.M150_PRICE)) * parseInt(f.colli);
                furnitureExtra += t2;
            }
            if (f.bulky && !f.extraPrice) {
                let t3 = parseFloat(MasterData.getOption(OPTIONS.BULKY_PRICE)) * parseInt(f.colli);
                furnitureExtra += t3;
            }
        });

    return furnitureExtra;
};
