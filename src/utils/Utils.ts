export namespace Utils {
    export const formatDate = (date: Date, format: DateFormat): string => {
        const day: string = date.getDate() < 10 ? "0" + date.getDate() : date.getDate().toString();
        const month: string = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
        const year: string = date.getFullYear().toString();

        switch (format) {
            case DateFormat.ddMMyyyy: {
                return `${day}.${month}.${year}`;
            }

            default: {
                return `${year}-${month}-${day}`;
            }
        }
    };

    export const replaceAll = (str: string, search: string, replacement: string): string => {
        return str.replace(new RegExp(search, "g"), replacement);
    };

    export const generateRandomString = (length: number = 35): string => {
        let id = "i";
        let possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i <= length; i++) {
            id += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return id;
    };

    export const copyObject = <T>(object: object): T => {
        const copy: T = <T>{};
        Object.keys(object).forEach(key => {
            //@ts-ignore
            copy[key] = object[key];
        });
        return copy;
    };

    export const getISODateString = (): string => {
        let date = new Date();
        let offset = date.getTimezoneOffset() / 60;
        date.setHours(date.getHours() - offset);
        return date.toISOString();
    };
}

export enum DateFormat {
    yyyyMMdd = "yyyy-MM-dd",
    ddMMyyyy = "dd.MM.yyyy"
}
