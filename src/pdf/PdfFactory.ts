import * as jspdf from "jspdf";
require("jspdf-autotable");

import { autoTable as AutoTable } from "jspdf-autotable";

export default class PdfFactory {
    static readonly dim_x = 210;
    static readonly dim_y = 297;
    static readonly pt_mm_factor = 0.353;
    doc: jspdf;
    private filename: string;
    private readonly default_fontsize = 10;
    private x: number;
    y: number;
    private maxWidth: number;
    private maxHeight: number;
    private margin: Margin;

    constructor(filename: string, margin: Margin) {
        this.filename = filename;
        this.margin = margin;
        this.doc = new jspdf("portrait", "pt", "a4");
        this.doc.setFontSize(this.default_fontsize);

        this.x = convertMmToPt(margin.left);
        this.y = convertMmToPt(margin.top);

        const widthMM = PdfFactory.dim_x - margin.right - margin.left;
        this.maxWidth = convertMmToPt(widthMM);

        const heightMM = PdfFactory.dim_y - margin.bottom - margin.top;
        this.maxHeight = convertMmToPt(heightMM);
    }

    public enumeratePages(text: string): void {
        let pageCount = this.doc.internal.getNumberOfPages();
        for (let i = 0; i < pageCount; i++) {
            this.doc.setPage(i);
            this.doc.setFontSize(7);
            this.doc.setTextColor(85, 85, 85);

            let txt = `Seite ${this.doc.internal.getCurrentPageInfo().pageNumber} von ${pageCount} | ${text}`;
            this.doc.text(txt, convertMmToPt(205), convertMmToPt(292), undefined, 0, "right");
        }
        this.resetText();
    }

    public getY(): number {
        return convertPtToMm(this.y);
    }

    public setBold(): void {
        this.doc.setFontStyle("bold");
    }

    public setNormal(): void {
        this.doc.setFontStyle("normal");
    }
    public setMarginLeft(margin: number): void {
        this.maxWidth = this.maxWidth - convertMmToPt(margin);
        this.x = this.x + convertMmToPt(margin);
    }

    public save(): void {
        this.doc.save(this.filename);
    }

    public addPage(margin?: Margin): void {
        if (margin) {
            this.margin = margin;
            const widthMM = PdfFactory.dim_x - margin.right - margin.left;
            this.maxWidth = convertMmToPt(widthMM);

            const heightMM = PdfFactory.dim_y - margin.bottom - margin.top;
            this.maxHeight = convertMmToPt(heightMM);
        }
        this.doc.addPage("a4", "p");
        this.x = convertMmToPt(this.margin.left);
        this.y = convertMmToPt(this.margin.top);
    }

    public setColor(r: number, g: number, b: number): void {
        this.doc.setTextColor(r, g, b);
    }

    public addHeader(text: string, fontSize?: number, align?: string): void {
        const _fontSize = fontSize ? fontSize : 16;
        this.setBold();
        this.doc.setTextColor(216, 63, 3);
        this.addText(text, _fontSize, undefined, align);
        this.resetText();
    }

    public addBlackHeader(text: string): void {
        this.addText(text, 12);
        this.resetText();
    }

    public addSpace(mm?: number): void {
        this.y += convertMmToPt(mm || 8);
    }

    public addPngImage(url: string, x: number, y: number, width: number, height: number): void {
        let img = new Image(width, height);
        img.src = url;
        this.doc.addImage(img, "PNG", convertMmToPt(x), convertMmToPt(y), convertMmToPt(width), convertMmToPt(height));
    }

    public addLine(): void {
        let maxX = convertMmToPt(PdfFactory.dim_x - this.margin.right);
        this.doc.setDrawColor(217, 217, 217);
        this.doc.line(this.x, this.y, maxX, this.y);
        this.y += 5;
        this.resetText();
    }

    public addTop(left: string[], right: string[]): void {
        this.doc.setTextColor(85, 85, 85);
        this.addLeftRight(left, right, 8);
        this.resetText();
    }

    public addTable(head: any, body: any): void {
        let bottom = convertMmToPt(10);
        ((this.doc as any).autoTable as AutoTable)({
            head: head,
            body: body,
            theme: "grid",
            headStyles: { fillColor: [240, 240, 240], textColor: 0 },
            bodyStyles: { halign: "left", textColor: [0, 0, 0] },
            styles: { fontSize: 9, cellPadding: 2 },
            startY: this.y,
            margin: { top: convertMmToPt(10), right: convertMmToPt(12), bottom, left: convertMmToPt(25) }
        });
        this.y = (this.doc as any).lastAutoTable.finalY + convertMmToPt(5);
    }

    public addLeftRight(left: string[], right: string[], fontSize?: number): void {
        const fs = fontSize ? fontSize : 10;
        const lastY = this.y;
        const lastX = this.x;

        const lh = fs / 2 + 2;
        left.forEach(line => {
            this.addText(line, fs, lh);
        });
        this.y = lastY;
        this.x = convertMmToPt(PdfFactory.dim_x - this.margin.right);
        right.forEach(line => {
            this.addText(line, fs, lh, "right");
        });
        this.setMarginLeft(-10);

        this.x = lastX;
    }

    public add2Cols(left: string[], right: string[], fontSize?: number, lh?: number, margin?: number): void {
        const lastY = this.y;
        const lastX = this.x;
        this.setMarginLeft(margin ? margin : 10);
        const fs = fontSize ? fontSize : 10;

        const _lh = lh ? lh : fs / 2 + 2;
        left.forEach(line => {
            this.addText(line, fs, _lh);
        });
        const leftY = this.y;
        this.y = lastY;
        this.x = convertMmToPt(PdfFactory.dim_x / 2);
        right.forEach(line => {
            this.addText(line, fs, _lh);
        });
        this.x = lastX;
        this.y = leftY > this.y ? leftY : this.y;
    }

    public addFooter(text: string, text2: string): void {
        let lastY = this.y;
        this.y = convertMmToPt(convertPtToMm(this.maxHeight) - 14);
        this.addLine();
        this.doc.setTextColor(85, 85, 85);
        this.addText(text, 8, 6, "center");
        this.doc.setTextColor(85, 85, 85);

        this.addText(text2, 8, 6, "center");
        this.resetText();
        this.y = lastY;
    }

    public addTextNoSpace(text: string, fontSize?: number, lh?: number): void {
        if (fontSize) {
            this.doc.setFontSize(fontSize);
        }
        const dims: Dims = this.doc.getTextDimensions(text);
        lh = lh ? lh : this.doc.getLineHeight();
        const nexty =
            Math.ceil(dims.w / (this.maxWidth - convertMmToPt(0))) * (fontSize ? fontSize : this.default_fontsize) +
            this.y +
            (fontSize ? fontSize : this.default_fontsize);
        if (nexty <= this.maxHeight) {
            this.doc.text(text, this.x, this.y, { maxWidth: this.maxWidth });
            this.y = nexty;
        } else {
            this.doc.addPage("a4", "p");
            this.x = convertMmToPt(this.margin.left);
            this.y = convertMmToPt(this.margin.top);
            this.doc.text(text, this.x, this.y, { maxWidth: this.maxWidth });
            // const lh = this.doc.getLineHeight();
            const nexty = Math.ceil(dims.w / this.maxWidth) * lh + this.y + lh / 2;
            this.y = nexty;
        }
    }

    public addText(text: string, fontSize?: number, lh?: number, align?: string): void {
        let lastX = this.x;
        let _align = align ? align : "left";
        switch (_align) {
            case "center":
                this.x = convertMmToPt(PdfFactory.dim_x / 2);
                break;
            case "right":
                this.x = convertMmToPt(PdfFactory.dim_x - this.margin.right);
                break;
            default:
                break;
        }
        if (fontSize) {
            this.doc.setFontSize(fontSize);
        }
        const dims: Dims = this.doc.getTextDimensions(text);
        lh = lh ? lh : this.doc.getLineHeight();
        const nexty = Math.ceil(dims.w / this.maxWidth) * lh + this.y + lh;
        if (nexty <= this.maxHeight) {
            this.doc.text(text, this.x, this.y + lh, { maxWidth: this.maxWidth, align: _align });
            this.y = nexty;
        } else {
            this.doc.addPage("a4", "p");
            // this.x = convertMmToPt(this.margin.left);
            // if (this.marginLeft >= 0) {
            //     this.x += convertMmToPt(this.marginLeft);
            // }
            this.y = convertMmToPt(this.margin.top);
            this.doc.text(text, this.x, this.y, { maxWidth: this.maxWidth, align: _align });
            const lh = this.doc.getLineHeight();
            const nexty = Math.ceil(dims.w / this.maxWidth) * lh + this.y + lh / 2;
            this.y = nexty;
        }
        this.x = lastX;
    }

    public resetText(): void {
        this.doc.setFontSize(this.default_fontsize);
        this.doc.setTextColor(0, 0, 0);
        this.setNormal();
    }
}
export const convertPtToMm = (pt: number): number => {
    return pt * PdfFactory.pt_mm_factor;
};

export const convertMmToPt = (mm: number): number => {
    return mm / PdfFactory.pt_mm_factor;
};

interface Margin {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

interface Dims {
    h: number;
    w: number;
}
