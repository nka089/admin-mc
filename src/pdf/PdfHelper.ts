import { OPTIONS } from "src/constants/Constants";
import PdfFactory, { convertMmToPt } from "src/pdf/PdfFactory";
import { MasterData } from "src/processes/MasterDataProceses";
import { Customer, Order } from "src/typings/types";
import pdfConfig from "./PdfConfig";
import { autoTable as AutoTable, UserOptions } from "jspdf-autotable";

export const generatePDF = (order: Order, isTime?: boolean): void => {
    const filename =
        `${order.date || order.date_from}` +
        `_${order.customer.lastName}_${order.id}_${order.workersNumber || 0}-${order.transporterNumber || 0}-${order.t75 || 0}.pdf`;

    const pdffactory = new PdfFactory(filename, { left: 20, right: 12, top: 8, bottom: 3 });
    pdffactory.addFooter(pdfConfig.firma.bank.join(" | "), pdfConfig.firma.adress.join(" | "));
    pdffactory.addPngImage(pdfConfig.firma.logoUrl, 18, 5, 30, 27);
    pdffactory.addTop([], pdfConfig.firma.contact);
    addMain(pdffactory, order);
    addCustomer(pdffactory, order.customer);
    addAddresses(pdffactory, order);
    addServiceDescription(pdffactory, order);
    if (order.text) {
        addMessage(pdffactory, order);
    }
    addPrice(pdffactory, order, isTime);
    addServices(pdffactory, order);
    addUsm(pdffactory, order);
    addEnding(pdffactory);

    addItems(pdffactory, order);
    addAGB(pdffactory);
    addEnumerations(pdffactory, order);
    pdffactory.save();
};

const addMain = (pdffactory: PdfFactory, order: Order): void => {
    pdffactory.addSpace();
    pdffactory.addBlackHeader(`Auftrag/Angebot Nr.: ${order.id}`);
    pdffactory.setMarginLeft(5);
    pdffactory.setBold();
    pdffactory.addText(`Umzugsdatum: ${order.isDateFix ? order.date : `${order.date_from} - ${order.date_to}`}, ab ${order.time} Uhr`, 9);
    pdffactory.addText(`Umzugsvolumen: ${order.volume} m³`, 9);
    if (typeof order.transporterNumber !== "undefined" || typeof order.workersNumber !== "undefined") {
        pdffactory.addText(`Ladehelfer: ${order.workersNumber}, LKW 3,5t: ${order.transporterNumber || 0}, LKW 7,5t: ${order.t75 || 0} `);
    }
    pdffactory.setNormal();
    pdffactory.setMarginLeft(-5);
    pdffactory.addLine();
};

const addEnumerations = (factory: PdfFactory, order: Order): void => {
    let name = order.customer.company || order.customer.lastName;
    factory.enumeratePages(`${order.id} | ${name}`);
};

const addAGB = (factory: PdfFactory): void => {
    let left = [
        `Bett Demontage oder Montage: ${MasterData.getOption(OPTIONS.A_MONTAGE_BET)} € `,
        `Küche Demontage je 1 m.: ${MasterData.getOption(OPTIONS.A_KITCHEN_MONTAGE)} € `,
        `Schrank Demontage oder Montage je 1 m.: ${MasterData.getOption(OPTIONS.A_WARDROBE_MONTAGE)} €`,
        `Je zusätzliches m³ Ladevolumen: ${MasterData.getOption(OPTIONS.A_CBM)} € `
    ];
    let right = [
        `Je 10 Meter zusätzlichem Laufweg am Auzugsort oder Einzugsort: ${MasterData.getOption(OPTIONS.A_10_METER)} €`,
        `1 Karton zusätzlich Einpacken oder Auspacken: ${MasterData.getOption(OPTIONS.A_KARTON_PACK)} €`,
        `Jede zusätzliche Etage am Auzugsort oder Einzugsort: ${MasterData.getOption(OPTIONS.A_ETAGE)} € `,

        `Entsorgung: ${MasterData.getOption(OPTIONS.DISPOSAL_CBM)} €/m³ zzgl. einmaliger Pauschale in Höhe von ${MasterData.getOption(
            OPTIONS.DISPOSAL_PAUSCHALE
        )} €`
    ];
    factory.addPage({ top: 3, left: 8, right: 8, bottom: 3 });
    factory.addHeader("Allgemeine Geschäftsbedingungen (AGB) der Durchführung des Umzugs", 10);
    addAGBPart(factory, pdfConfig.agb0);
    factory.add2Cols(left, right, 6.5, 4, 5);
    factory.addSpace(1);
    addAGBPart(factory, pdfConfig.agb2);
};

const addAGBPart = (factory: PdfFactory, pars: string[][]): void => {
    let bold = true;
    pars.forEach(text => {
        if (bold) {
            factory.setBold();
        } else {
            factory.setNormal();
        }
        bold = !bold;
        factory.addTextNoSpace(text[0], 7);
    });
};

const addSignature = (factory: PdfFactory, sign: string): void => {
    factory.addText("___________________", 8, 6, "right");
    factory.addText(sign, 8, 4, "right");
    factory.resetText();
};

const addEnding = (factory: PdfFactory): void => {
    let fs = 9;
    factory.addHeader("Vor dem Umzug", 11);
    factory.setMarginLeft(5);
    factory.addText(
        "• Zahlungsart: An unsere Mitarbeiter vor Ort nach dem Umzug in Bar oder per Überweisung (Zahlungseingang auf unser Konto spätestens 1 Tag vor dem Umzug)",
        fs
    );
    factory.addText("• Die Preise sind inklusive der gesetzlichen Haftung von 620 Euro/m³", fs);
    factory.addText(
        '• Werden der Firma  Umzug Meister Details des Umzuges nicht bekannt gegeben, behält sich das Unternehmen das Recht vor, den Auftrag, bzw. die Möbelliste um Positionen wie z.B. "Volumen CBM", "Schwertransport, "Trageweg" oder "Transport von sperrigen Gegenständen"  zu erweitern. Die Kosten sind vom Kunden zu tragen.',
        fs
    );
    factory.setBold();
    factory.addText(
        "Hiermit erteile(n) ich(wir) den Auftrag, den Umzug zum o.g. Konditionen durchzuführen. Ich(wir) akzeptiere(n) die AGB sowie HGB der Firma Umzug Meister.",
        fs
    );
    factory.setMarginLeft(-5);
    factory.setNormal();
    addSignature(factory, "Kundenunterscrift");
    if (PdfFactory.dim_y - factory.getY() < 40) {
        factory.addPage();
        factory.setMarginLeft(5);
    }
    factory.addLine();
    factory.addHeader("Nach dem Umzug", 11);
    factory.setMarginLeft(5);

    factory.addText("Nach der Besichtigung wurden Mängel festgestellt: ", 9);
    factory.addText("[  ] keine", fs, 5);
    factory.addText("[  ] Möbel, und zwar: ", fs, 5);
    factory.addText("[  ] Fußböden, und zwar: ", fs, 5);
    factory.addText("[  ] Wände, und zwar:  ", fs, 5);
    factory.setBold();
    factory.addText("Auftrag vollständig ausgeführt:", fs);
    factory.setNormal();
    addSignature(factory, "Kundenunterscrift");
    factory.setBold();
    factory.addText("Gesamtbetrag dankend erhalten:", fs);
    factory.setNormal();
    addSignature(factory, "Unterschrift des Fahrers");
    factory.resetText();
};

const addAddresses = (factory: PdfFactory, order: Order): void => {
    factory.addLine();
    factory.addHeader(`Adressen`, 11);
    let head = [{ desc: "", from: "Auszug", to: "Einzug" }];
    let to = order.to.address.split(",");
    let from = order.from.address.split(",");
    let body = [
        { desc: "Strasse, Nr:", from: from[0] ? from[0] : "", to: to[0] ? to[0] : "" },
        { desc: "PLZ, Ort:", from: from[1] ? from[1].trim() : "", to: to[1] ? to[1].trim() : "" },
        { desc: "Etage:", from: order.from.floor, to: order.to.floor },
        { desc: "Aufzug:", from: order.from.liftType, to: order.to.liftType },
        { desc: "Trageweg:", from: `${order.from.runningDistance}`, to: `${order.to.runningDistance}` },
        {
            desc: "Parkmöglichkeit:",
            from: `${order.from.parkingSlot ? "HVZ" : "Organisiert d. Kunde"}`,
            to: `${order.to.parkingSlot ? "HVZ" : "Organisiert d. Kunde"}`
        },
        { desc: "Dachboden:", from: `${order.from.hasLoft ? "Ja" : "Nein"}`, to: `${order.to.hasLoft ? "Ja" : "Nein"}` },
        { desc: "Packservice:", from: `${order.from.packservice ? "Einpacken" : "Nein"}`, to: `${order.to.packservice ? "Auspacken" : "Nein"}` },
        { desc: "Umzugsobjekt:", from: order.from.movementObject, to: order.to.movementObject }
    ];

    let bottom = convertMmToPt(10);
    //@ts-ignore
    ((factory.doc as any).autoTable as AutoTable)({
        //@ts-ignore
        head: head,
        //@ts-ignore
        body: body,
        theme: "grid",
        headStyles: { fillColor: [240, 240, 240], textColor: 0 },
        bodyStyles: { halign: "left", textColor: [0, 0, 0] },
        styles: { fontSize: 9, cellPadding: 2 },
        startY: factory.y,
        columnStyles: { desc: { fontStyle: "bold" } },
        margin: { top: convertMmToPt(10), right: convertMmToPt(12), bottom, left: convertMmToPt(25) }
    });
    factory.y = (factory.doc as any).lastAutoTable.finalY + convertMmToPt(5);
};

const addCustomer = (factory: PdfFactory, customer: Customer): void => {
    const lines: string[] = [];
    if (customer.company) {
        lines.push(`${customer.company}`);
    }
    lines.push(`${customer.salutation ? customer.salutation : ""} ${customer.firstName ? customer.firstName : ""} ${customer.lastName}`);
    if (customer.telNumber) {
        lines.push(`Tel: ${customer.telNumber}`);
    }
    factory.addHeader("Auftraggeber/in", 11);
    factory.setMarginLeft(5);
    lines.forEach(line => factory.addText(line, 9, 6));
    factory.setMarginLeft(-5);
};

const addServiceDescription = (factory: PdfFactory, order: Order): void => {
    factory.addLine();
    factory.addHeader(`Leistungen`, 11);
    factory.setMarginLeft(5);
    let lh = 6;
    let fs = 9;
    factory.addText(`• Transport von ${order.volume} m³ Umzugsgut von Auszugs- zu Einzugsadresse.`, fs, lh);
    factory.addText(`• Kosten für die Anfahrt, Lastfahrt sowie Abfahrt. Gesamtstrecke: ${order.distance} km.`, fs, lh);
    let demontage = false;
    for (let j = 0; j < order.items.length; j++) {
        let i = order.items[j];
        if (i.demontage === true || i.montage === true) {
            demontage = true;
            break;
        }
    }
    if (order.boxNumber && order.boxNumber > 0) {
        factory.addText(`• Tranport von ca. ${order.boxNumber} Umzugskartons von Auszugs- zu Einzugsadresse.`, fs, lh);
    }
    if (demontage === true) {
        factory.addText(`• Möbel Demontage bzw. Montage gemäß der Umzugsgutliste.`, fs, lh);
    }
    const cbmPrice = MasterData.getOption(OPTIONS.DISPOSAL_CBM);
    const disposal = MasterData.getOption(OPTIONS.DISPOSAL_PAUSCHALE);
    if (order.usmFlag === true && order.usmText && order.usmText.length > 0) {
        factory.addText(`• Demontage/Montage der USM Möbel.`, fs, lh);
    }
    if (order.disposalFlag === true) {
        factory.addText(`• Entsorgung der Gegenstände zum folgenden Preis: ${cbmPrice} €/m³, zzgl. der Pauschale  i.H.v.: ${disposal} €.`, fs, lh);
    }
    if (order.from.parkingSlot === true || order.to.parkingSlot === true) {
        factory.addText(`• Organisation der Halteverbotszone(n).`, fs, lh);
    }
    if (order.from.packservice === true || order.to.packservice === true) {
        factory.addText(`• Packservice`, fs, lh);
    }
    let bohr = false;
    let pack = false;
    for (let i = 0; i < order.services.length; i++) {
        let s = order.services[i];
        if (s.tag === "Bohrarbeiten") {
            bohr = true;
        } else if (s.tag === "Packmaterial") {
            pack = true;
        }
    }
    if (bohr === true) {
        factory.addText(`• Bohrarbeiten gemäß der Bohrbeitenliste.`, fs, lh);
    }
    if (pack === true) {
        factory.addText(`• Bereitstellung/Abholung der Packmaterialien gemäß der Packmaterialliste.`, fs, lh);
    }
    factory.setMarginLeft(-5);
};

const addMessage = (factory: PdfFactory, order: Order): void => {
    factory.addLine();
    factory.addHeader(`Kundennachricht/Besondere Vereinbarungen`, 11);
    factory.setMarginLeft(5);
    factory.setColor(2, 103, 143);
    factory.addText(order.text, 9);
    factory.setMarginLeft(-5);
    factory.resetText();
};

const addPrice = (factory: PdfFactory, order: Order, isTime?: boolean): void => {
    factory.addLine();
    factory.addHeader(`Preis`, 11);
    let price = parseFloat(order.sum);
    let mwst = ((price / 119) * 19).toFixed(2);
    let netto = price - parseFloat(mwst);
    factory.setBold();
    if (isTime === true) {
        price = parseFloat(order.timeBased.basis);
        mwst = ((price / 119) * 19).toFixed(2);
        netto = price - parseFloat(mwst);
        factory.addText(`Netto: ${netto.toFixed(2)} €`, 9, 6, "right");
        factory.addText(`MwSt. 19%: ${mwst} €`, 9, 6, "right");
        factory.addText(`Gesamtpreis für ${order.timeBased.hours} Stunden: ${price.toFixed(2)} €`, 9, 6, "right");
        factory.addText(`Je angefangene weitere Stunde: ${order.timeBased.extra} € inkl. MwSt.`, 9, 6, "right");
    } else {
        factory.addText(`Netto: ${netto.toFixed(2)} €`, 9, 6, "right");
        factory.addText(`MwSt. 19%: ${mwst} €`, 9, 6, "right");
        factory.addText(`Gesamt: ${price.toFixed(2)} €`, 9, 6, "right");
    }
    factory.setNormal();
};

const addServices = (factory: PdfFactory, order: Order): void => {
    factory.addPage();
    if (PdfFactory.dim_y - factory.getY() < 20) {
        factory.addPage();
    }
    const bohr = order.services.filter(s => s.tag === "Bohrarbeiten" && s.colli !== "0" && s.colli);
    if (bohr.length > 0) {
        factory.addHeader("Bohrarbeitenliste", 11);
        const header = [["Bohrarbeitenart", "Einzelpreis, in €", "Anzahl"]];

        const body = new Array<Array<string>>();

        bohr.forEach(s => {
            if (s.colli && s.colli !== "0") {
                body.push([s.name, s.price, s.colli]);
            }
        });
        factory.addTable(header, body);
        factory.addLine();
    }

    const pack = order.services.filter(s => s.tag === "Packmaterial" && s.colli !== "0" && s.colli);
    if (pack.length > 0) {
        factory.addHeader("Packmaterialliste", 11);
        const header = [["Artikel", "Einzelpreis, in €", "Anzahl"]];

        const body = new Array<Array<string>>();

        pack.forEach(s => {
            if (s.colli && s.colli !== "0") {
                body.push([s.name, s.price, s.colli]);
            }
        });
        factory.addTable(header, body);
        factory.addLine();
    }
    if (order.disposalFlag === true) {
        factory.addHeader("Entsorgung", 11);
        if (order.disposalText) {
            factory.setMarginLeft(5);
            factory.addText(order.disposalText, 9);
            factory.setMarginLeft(-5);
        }
        factory.addLine();
    }
};

const addItems = (factory: PdfFactory, order: Order): void => {
    const { items } = order;
    items.sort((i1, i2) => {
        if (i2.categories[0] < i1.categories[0]) {
            return -1;
        }
        if (i2.categories[0] > i1.categories[0]) {
            return 1;
        }
        return 0;
    });
    if (items.length > 0) {
        factory.addPage();
        factory.addHeader("Umzugsgutliste", 11);
        const header = [
            {
                desc: "Bezeichnung",
                colli: "Anzahl",
                demontage: "Demontage",
                montage: "Montage",
                notDismountable: "Nicht zerlegbar",
                bulky: "Sperrig",
                m100: "> 100 kg",
                m150: "> 150 kg"
            }
        ];

        const body = [];

        let room = "";
        let item;
        for (let i = 0; i < items.length; i++) {
            item = items[i];
            if (item.colli === "0") {
                continue;
            }
            let actualroom = "";
            if (item.categories[0] !== room) {
                room = item.categories[0];
                actualroom = item.categories[0];
                body.push({
                    desc: {
                        colSpan: 8,
                        content: actualroom,
                        //rgb(2,105,143)
                        // styles: { fontStyle: "bold", halign: "left", textColor: [0, 221, 94] }
                        styles: { fontStyle: "bold", halign: "left", textColor: [2, 105, 143] }
                    }
                });
            }
            const row: any = {};
            const _true = "x";
            row.desc = item.name;
            row.colli = item.colli;
            row.montage = item.montage ? _true : "";
            row.demontage = item.demontage ? _true : "";
            row.notDismountable = item.notDismountable ? _true : "";
            row.bulky = item.bulky ? _true : "";
            row.m100 = item.m100 ? _true : "";
            row.m150 = item.m150 ? _true : "";
            body.push(row);
        }

        let bottom = convertMmToPt(10);
        //@ts-ignore
        ((factory.doc as any).autoTable as AutoTable)({
            head: header,
            body: body,
            theme: "grid",
            headStyles: { fillColor: [240, 240, 240], textColor: 0 },
            bodyStyles: { halign: "center", textColor: 0 },
            styles: { fontSize: 9, cellPadding: 2 },
            startY: factory.y,
            columnStyles: { desc: { halign: "left" } },

            margin: { top: convertMmToPt(10), right: convertMmToPt(12), bottom, left: convertMmToPt(25) }
        } as UserOptions);
        factory.y = (factory.doc as any).lastAutoTable.finalY + convertMmToPt(5);
        if (order.boxNumber) {
            factory.setBold();
            factory.setMarginLeft(5);
            factory.addText(`Anzahl der Umzugskartons: ca. ${order.boxNumber}`, 9);
            factory.setMarginLeft(-5);
            factory.setNormal();
        }
        factory.addLine();
    }
};

const addUsm = (factory: PdfFactory, order: Order): void => {
    if (order.usmFlag === true) {
        factory.addHeader("USM Demontage/Montage", 11);
        if (order.usmText) {
            factory.setMarginLeft(5);
            factory.addText(order.usmText, 9);
            factory.setMarginLeft(-5);
        }
        factory.addLine();
    }
};
