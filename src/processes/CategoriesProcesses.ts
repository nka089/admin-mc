import { createProcess } from "@dojo/framework/stores/process";
import { replace } from "@dojo/framework/stores/state/operations";
import Store from "@dojo/framework/stores/Store";
import { ServicePaths } from "../restservice/ServicePaths";
import { State } from "../typings/store";
import { Category } from "../typings/types";
import { createCommand } from "./processUtils";
import HTTPRequest from "../restservice/HTTPRequest";
import { CONTROL } from "./Processes";

const restService = new HTTPRequest();
export const initCategories = async (store: Store<State>): Promise<void> => {
    try {
        const categories = await restService.getRequest(ServicePaths.CATEGORY_ALL);
        setCategoriesProcess(store)(categories);
    } catch (error) {
        console.log(error);
    }
};

export const deleteCategory = async (store: Store<State>, id: string): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        await restService.deleteRequest(ServicePaths.CATEGORY_BY_ID(id));
        deletCategoryProcess(store)(id);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

export const createCategory = async (store: Store<State>, name: string): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);

        const newCategory = await restService.postRequest(ServicePaths.CATEGORY, { name: name });
        await addCategoryProcess(store)(newCategory);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

const deleteCategoryCommand = createCommand<String>(({ get, path, payload }) => {
    const arr = get(path("categories"));
    const newarr = arr.filter(c => c.id !== payload.valueOf());
    return [replace(path("categories"), newarr)];
});
const addCategory = createCommand<Category>(({ get, path, payload }) => {
    const arr = get(path("categories"));
    arr.push(payload);
    const newarr = arr;
    return [replace(path("categories"), newarr)];
});

const setAllCategories = createCommand<Category[]>(({ path, payload }) => {
    return [replace(path("categories"), payload)];
});

const setCategoriesProcess = createProcess("setCategoriesProcess", [setAllCategories]);
const addCategoryProcess = createProcess("addCategoryProcess", [addCategory]);
const deletCategoryProcess = createProcess("deletCategoryProcess", [deleteCategoryCommand]);
