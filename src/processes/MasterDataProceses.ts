import { _store } from "src/main";
import { OPTIONS } from "src/constants/Constants";

export namespace MasterData {
    export const getOption = (name: OPTIONS): string => {
        const _s = _store();
        const { get, path } = _s;
        const allOpts = get(path("options"));
        return allOpts[name] || " ";
    };
}
