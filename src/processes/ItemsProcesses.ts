import { createProcess } from "@dojo/framework/stores/process";
import { replace } from "@dojo/framework/stores/state/operations";
import Store from "@dojo/framework/stores/Store";
import { ServicePaths } from "../restservice/ServicePaths";
import { State } from "../typings/store";
import { createCommand } from "./processUtils";
import HTTPRequest from "../restservice/HTTPRequest";
import { Item } from "../typings/types";
import { CONTROL } from "./Processes";

const restService = new HTTPRequest();
export const initItems = async (store: Store<State>): Promise<void> => {
    try {
        const items = await restService.getRequest(ServicePaths.ITEM_ALL);

        await setItemsProcess(store)(items);
    } catch (error) {
        console.log(error);
    }
};

export const deleteItem = async (store: Store<State>, id: string): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        await restService.deleteRequest(ServicePaths.ITEM_BY_ID(id));
        deleteItemProcess(store)(id);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

export const createItem = async (store: Store<State>, item: Item): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);

        const newItem = await restService.postRequest(ServicePaths.ITEM, item);
        await addItemProcess(store)(newItem);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

export const updateItem = async (store: Store<State>, item: Item): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        const updatedItem = await restService.putRequest(ServicePaths.ITEM_BY_ID(item.id), item);
        replaceItemProcess(store)(updatedItem);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

const deleteItemCommand = createCommand<String>(({ get, path, payload }) => {
    const arr = get(path("items"));
    const newarr = arr.filter(c => c.id !== payload.valueOf());
    return [replace(path("items"), newarr)];
});
const addItem = createCommand<Item>(({ get, path, payload }) => {
    const arr = get(path("items"));
    arr.push(payload);
    const newarr = arr;
    return [replace(path("items"), newarr)];
});

const replaceItemCommand = createCommand<Item>(({ at, get, path, payload }) => {
    const arr = get(path("items"));

    const index = arr.findIndex(item => item.id === payload.id);
    if (index > -1) {
        return [replace(at(path("items"), index), payload)];
    } else {
        return [];
    }
});

const setAllItems = createCommand<Item[]>(({ path, payload }) => {
    let items = payload.filter(i => {
        if (i.categoryRefs) {
            return true;
        } else {
            console.log("Please delete item width id", i.id);
            console.log(i);
            return false;
        }
    });
    items.forEach(i => (i.colli = "0"));
    return [replace(path("items"), items)];
});

const setItemsProcess = createProcess("setCategoriesProcess", [setAllItems]);
const addItemProcess = createProcess("addCategoryProcess", [addItem]);
const deleteItemProcess = createProcess("deletCategoryProcess", [deleteItemCommand]);
const replaceItemProcess = createProcess("replaceItemProcess", [replaceItemCommand]);
