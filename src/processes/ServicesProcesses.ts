import HTTPRequest from "../restservice/HTTPRequest";
import Store from "@dojo/framework/stores/Store";
import { State } from "../typings/store";
import { ServicePaths } from "../restservice/ServicePaths";
import { Service } from "../typings/types";
import { createCommand } from "./processUtils";
import { replace } from "@dojo/framework/stores/state/operations";
import { createProcess } from "@dojo/framework/stores/process";
import { CONTROL } from "./Processes";

const restService = new HTTPRequest();

export const initServices = async (store: Store<State>): Promise<void> => {
    try {
        const services = await restService.getRequest(ServicePaths.SERVICE_ALL);
        setCategoriesProcess(store)(services);
    } catch (error) {
        console.log(error);
    }
};

export const deleteService = async (store: Store<State>, id: string): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        await restService.deleteRequest(ServicePaths.SERVICE_BY_ID(id));
        deletServiceProcess(store)(id);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

export const updateService = async (store: Store<State>, service: Service): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        delete service.colli;
        const updatedService = await restService.putRequest(ServicePaths.SERVICE_BY_ID(service.id), service);
        replaceServiceProcess(store)(updatedService);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

export const createService = async (store: Store<State>, service: Service): Promise<void> => {
    try {
        CONTROL.setShowHourlassProcess(store)(true);
        const newService = await restService.postRequest(ServicePaths.SERVICE, service);

        await addServiceProcess(store)(newService);
    } catch (error) {
        console.error(error);
    } finally {
        CONTROL.setShowHourlassProcess(store)(false);
    }
};

const replaceServiceCommand = createCommand<Service>(({ at, get, path, payload }) => {
    const arr = get(path("services"));
    const index = arr.findIndex(service => service.id === payload.id);
    if (index > -1) {
        return [replace(at(path("services"), index), payload)];
    } else {
        return [];
    }
});

const deleteServiceCommand = createCommand<String>(({ get, path, payload }) => {
    const arr = get(path("services"));
    const newarr = arr.filter(service => service.id !== payload.valueOf());
    return [replace(path("services"), newarr)];
});

const addService = createCommand<Service>(({ get, path, payload }) => {
    const arr = get(path("services"));
    arr.push(payload);
    const newarr = arr;
    return [replace(path("services"), newarr)];
});

const setAllServices = createCommand<Service[]>(({ path, payload }) => {
    return [replace(path("services"), payload)];
});

const replaceServiceProcess = createProcess("replaceItemProcess", [replaceServiceCommand]);
const setCategoriesProcess = createProcess("setCategoriesProcess", [setAllServices]);
const addServiceProcess = createProcess("addServiceProcess", [addService]);
const deletServiceProcess = createProcess("deletServiceProcess", [deleteServiceCommand]);
