import { createCommandFactory } from "@dojo/framework/stores/process";
import { State } from "../typings/store";

export const createCommand = createCommandFactory<State>();

export function handleGetRequestError<T>(error: any, isArray: boolean = false): Promise<T> | Promise<T[]> {
    console.warn(error);
    if (isArray) {
        const array: T[] = [];
        return Promise.resolve(array);
    } else {
        return Promise.resolve(<T>{});
    }
}
