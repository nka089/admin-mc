import { createProcess } from "@dojo/framework/stores/process";
import { replace } from "@dojo/framework/stores/state/operations";
import HTTPRequest from "../restservice/HTTPRequest";
import { ServicePaths } from "../restservice/ServicePaths";
import { createCommand } from "./processUtils";

const restService = new HTTPRequest();

const initOptionsCommand = createCommand(async ({ path }) => {
    const options: { [key: string]: string } = await restService.getRequest(ServicePaths.ALL_OPTIONS);
    return [replace(path("options"), options)];
});

export const initOptionsProcess = createProcess("init-options-process", [initOptionsCommand]);

export namespace CONTROL {
    const setShowDialogCommand = createCommand<Boolean>(({ path, payload }) => [replace(path("control", "showDialog"), payload)]);
    export const setShowDialog = createProcess("setShowDialog", [setShowDialogCommand]);

    const setShowHourglassCommand = createCommand<Boolean>(({ path, payload }) => [replace(path("control", "showHourglass"), payload)]);
    export const setShowHourlassProcess = createProcess("setShowHourlassProcess", [setShowHourglassCommand]);

    const setLastCahcheKeyCommand = createCommand<String>(({ path, payload }) => [replace(path("control", "lastCacheKey"), payload)]);
    export const setLastCahcheKeyProcess = createProcess("setLastCahcheKeyProcess", [setLastCahcheKeyCommand]);
}
