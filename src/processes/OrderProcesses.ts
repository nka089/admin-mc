import { createProcess } from "@dojo/framework/stores/process";
import { replace } from "@dojo/framework/stores/state/operations";
import Store from "@dojo/framework/stores/Store";
import { to } from "src/main";
import { DataPayload } from "src/typings/payloads";
import { Item, Service } from "src/typings/types";
import { Utils } from "src/utils/Utils";
import HTTPRequest from "../restservice/HTTPRequest";
import { ServicePaths } from "../restservice/ServicePaths";
import { OrdersPayload } from "../typings/payloads";
import { State } from "../typings/store";
import { Order } from "../typings/types";
import { CONTROL } from "./Processes";
import { createCommand } from "./processUtils";

const setOrderRideCostsCommand = createCommand<String>(({ path, payload }) => [replace(path("order", "rideCosts"), payload.valueOf())]);

const setOrderVolumeCommand = createCommand<String>(({ path, payload }) => [replace(path("order", "volume"), payload.valueOf())]);
const setOrderVolumeCostsCommand = createCommand<String>(({ path, payload }) => [replace(path("order", "volumePrice"), payload.valueOf())]);

const setOrderPriceCommand = createCommand<String>(({ path, payload }) => [replace(path("order", "sum"), payload.valueOf())]);

const addServiceCommand = createCommand<Service>(({ get, path, payload }) => {
    const services = get(path("order", "services"));
    const mDataServices = get(path("services"));
    const service = services.find(s => payload.id === s.id);
    const mService = mDataServices.find(s => payload.id === s.id);
    if (mService) {
        mService.colli = payload.colli;
    }
    if (service) {
        service.colli = payload.colli;
    } else {
        services.push(payload);
    }
    return [];
});

const setDisposalText = createCommand<String>(({ path, payload }) => {
    const text = payload.valueOf().trim();
    if (text.length > 0) {
        return [replace(path("order", "disposalText"), text), replace(path("order", "disposalFlag"), true)];
    } else {
        return [replace(path("order", "disposalText"), text), replace(path("order", "disposalFlag"), false)];
    }
});

const setPersonalData = createCommand<DataPayload>(({ path, payload }) => {
    return [replace(path("order", "customer", payload.key), payload.value)];
});

const setDateFix = createCommand<Boolean>(({ path, payload }) => [replace(path("order", "isDateFix"), payload.valueOf())]);
const setText = createCommand<String>(({ path, payload }) => [replace(path("order", "text"), payload.valueOf())]);

const setItemsCommand = createCommand(({ get, path }) => {
    let items: Item[] = [];
    const itemblocks = get(path("appdata", "itemsBlocks"));
    itemblocks.forEach(block => {
        block.furniture
            .filter(furniture => furniture.colli && parseInt(furniture.colli) > 0)
            .forEach(furniture => {
                items.push(furniture);
            });
    });

    return [replace(path("order", "items"), items)];
});

export namespace OrderProcesses {
    export const setDisposalTextProcess = createProcess("setDisposalTextProcess", [setDisposalText]);
    export const setItemsProcess = createProcess("setItemsProcess", [setItemsCommand]);
    export const setDateFixProcess = createProcess("setDateFixProcess", [setDateFix]);
    export const setTextProcess = createProcess("setTextProcess", [setText]);
    export const setPersonalDataProcess = createProcess("setPersonalDataProcess", [setPersonalData]);
    export const addServiceProcess = createProcess("addServiceProcess", [addServiceCommand]);
    export const setOrderRideCosts = createProcess("setOrderRideCosts", [setOrderRideCostsCommand]);
    export const setOrderVolume = createProcess("setOrderVolume", [setOrderVolumeCommand]);
    export const setOrderVolumeCosts = createProcess("setOrderVolumeCosts", [setOrderVolumeCostsCommand]);
    export const setOrderPrice = createProcess("setOrderPrice", [setOrderPriceCommand]);
}

const restService = new HTTPRequest();

const setCurrentOrdersCommand = createCommand<OrdersPayload>(({ path, payload }) => [replace(path("currentOrders"), payload)]);

const setSelectedOrderCommand = createCommand<Order>(({ path, payload }) => {
    return [replace(path("order"), payload)];
});

const updateOrderCommand = createCommand<Order>(({ path, payload }) => [replace(path("order"), payload)]);
const triggerOrderUpdate = createCommand(({ path }) => [replace(path("control", "updateOrder"), Date.now())]);

export namespace ORDER {
    export const replaceOrderProcess = createProcess("replaceOrderProcess", [updateOrderCommand, triggerOrderUpdate]);
    export const setSelectedOrderProcess = createProcess("setSelectedOrderProcess", [setSelectedOrderCommand]);

    export const setCurrentordersProcess = createProcess("setCurrentordersProcess", [setCurrentOrdersCommand]);

    export const updateOrder = async (order: Order, store: Store<State>) => {
        try {
            CONTROL.setShowHourlassProcess(store)(true);
            order.items = order.items.filter(i => i.colli !== "0");
            order.services = order.services.filter(i => i.colli !== "0");

            order.lupd = Utils.getISODateString();

            if (order.id) {
                const updatedOrder: Order = await restService.putRequest(ServicePaths.ORDER_BY_ID(order.id), order);
                await replaceOrderProcess(store)(updatedOrder);
            } else {
                const newOrder = await restService.postRequest(ServicePaths.ORDER, order);
                console.log(newOrder);
                await replaceOrderProcess(store)(newOrder);
            }
        } catch (e) {
            alert("Fehler ist aufgetreten");
        } finally {
            // ORDER.invalidateCacheProcess(store)({});
            CONTROL.setShowHourlassProcess(store)(false);
        }
    };
    export const deleteOrder = async (store: Store<State>) => {
        try {
            CONTROL.setShowHourlassProcess(store)(true);
            const { get, path } = store;
            const id = get(path("order")).id!;
            await restService.deleteRequest(ServicePaths.ORDER_BY_ID(id));
            const orders = await ORDER.getOrderById(store, id);
            ORDER.setCurrentordersProcess(store)(orders);
            to("auftraege");
        } catch (e) {
            alert("Fehler ist aufgetreten");
        } finally {
            // ORDER.invalidateCacheProcess(store)({});
            CONTROL.setShowHourlassProcess(store)(false);
        }
    };

    export const getLastOrdders = async (nmb: string): Promise<Order[]> => {
        try {
            const orders: Order[] = await restService.getRequest(ServicePaths.LAST_ORDERS(nmb));
            if (orders && orders.length > 0) {
                return orders;
            } else {
                return [];
            }
        } catch (e) {
            console.log(e);

            return [];
        }
    };

    export const getOrderById = async (store: Store<State>, id: string): Promise<Order[]> => {
        // const {get, path} = store;
        // // const cachedOrders: Order[] = get(path("orderCache", "#".concat(id)));
        // if (cachedOrders && cachedOrders.length > 0) {
        //     return cachedOrders;
        // } else {
        try {
            const order: Order = await restService.getRequest(ServicePaths.ORDER_BY_ID(id));
            if (order) {
                // cacheOrdersProcess(store)({key: "#".concat(id), value: [order]});

                return [order];
            } else {
                return [];
            }
        } catch (e) {
            console.log(e);

            return [];
        }
        // }
    };

    export const getOrdersByNameOrdate = async (store: Store<State>, nameOrdate: string): Promise<Order[]> => {
        // const {get, path} = store;
        // const cachedOrders: Order[] = get(path("orderCache", "#".concat(nameOrdate)));
        // if (cachedOrders && cachedOrders.length > 0) {
        //     return cachedOrders;
        // } else {
        try {
            const orders: Order[] = await restService.getRequest(ServicePaths.SEARCH_ORDER_BY_DATE_OR_NAME(nameOrdate));
            if (orders && orders.length > 0) {
                // cacheOrdersProcess(store)({key: "#".concat(nameOrdate), value: orders});
                return orders;
            } else {
                return [];
            }
        } catch (e) {
            console.log(e);
            return [];
        }
        // }
    };
}
