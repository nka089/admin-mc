import { DNode } from "@dojo/framework/core/interfaces";
import { v, w } from "@dojo/framework/core/vdom";
import WidgetBase from "@dojo/framework/core/WidgetBase";
import { Outlet } from "@dojo/framework/routing/Outlet";
import AdminContainer from "src/containers/AdminContainer";
import Menu from "src/widgets/menu/Menu";
import OrderEditorView from "src/widgets/views/orderEditorView/OrderEditorView";
import CategoriesContainer from "../containers/CategoriesContainer";
import FurnitureContainer from "../containers/FurnitureContainer";
import ModalHourglassContainer from "../containers/ModalHourglassContainer";
import OptionsContainer from "../containers/OptionsContainer";
import OrdersContainer from "../containers/OrdersContainer";
import ServicesContainer from "../containers/ServicesContainer";

export default class Application extends WidgetBase {
    protected render(): DNode {
        return v("div", [
            w(Menu, {}),
            w(Outlet, {
                id: "auftraege",
                renderer: () => w(OrdersContainer, {})
            }),
            w(Outlet, {
                id: "auftrag-bearbeiten",
                renderer: () => w(OrderEditorView, {})
            }),

            w(Outlet, {
                id: "leistungen",
                renderer: () => w(ServicesContainer, {})
            }),

            w(Outlet, {
                id: "moebel",
                renderer: () => w(FurnitureContainer, {})
            }),

            w(Outlet, {
                id: "optionen",
                renderer: () => w(OptionsContainer, {})
            }),

            w(Outlet, {
                id: "kategorien",
                renderer: () => w(CategoriesContainer, {})
            }),
            w(Outlet, {
                id: "analytics",
                renderer: () => w(AdminContainer, {})
            }),
            w(ModalHourglassContainer, {})
        ]);
    }
}
