export interface Order {
    id: string | undefined;
    status: string;
    customer: Customer;
    date: string;
    date_from: string;
    date_to: string;
    isDateFix: boolean;
    from: Address;
    to: Address;
    distance: number;
    services: Service[];
    items: Item[];
    workersNumber: number;
    transporterNumber: number;
    t75: number;
    boxNumber: number;
    text: string;
    usmFlag: boolean;
    usmText: string;
    disposalFlag: boolean;
    disposalText: string;
    creationTime: string;
    lupd: string;
    volume: string;
    volumePrice: string;
    rideCosts: string;
    sum: string;
    discount: string;
    extraCbm: string;
    time: string;
    archived: boolean;
    timeBased: TimeBasedPrice;
    prices: Prices;
}

export interface TimeBasedPrice {
    hours: string;
    basis: string;
    extra: string;
}

export interface Prices {
    halteverbotszonen?: number;
    services?: number;
}

export interface MovementDates {
    date: string;
    date_from: string;
    date_to: string;
}

export interface Address {
    floor: string;
    roomsNumber: string;
    parkingSlot: boolean;
    area: string;
    personsNumber?: string;
    liftType: string;
    runningDistance: string;
    address: string;
    movementObject?: string;
    hasLoft: boolean;
    packservice: boolean;
}

export interface Customer {
    firstName: string;
    lastName: string;
    salutation: string;
    company: string;
    telNumber: string;
    email: string;
}

export interface Item {
    categoryRefs: Category[];
    categories: string[];
    name: string;
    id: string;
    colli: string;
    volume: number;
    demontage?: boolean;
    notDismountable?: boolean;
    bulky?: boolean;
    montage?: boolean;
    m100?: boolean;
    m150?: boolean;
    extraPrice?: string;
    montagePrice: string;
}

export interface Service {
    id: string;
    name: string;
    tag: string;
    description: string;
    price: string;
    colli: string;
    imgUrl: string;
}

export interface Category {
    id: string;
    slug: string;
    name: string;
}

export interface InputInformations {
    id?: number;
    date: string;
    originAddress: string;
    destinationAddress: string;
    distance: string;
}

export interface Option {
    key: string;
    value: string;
}
export interface ItemBlock {
    category: string;
    furniture: Item[];
}
