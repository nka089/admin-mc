import { Order } from "./types";

export interface OrderCachePayload {
    key: string;
    value: Order[];
}

export interface OrdersPayload extends Array<Order> {}

export interface AddressPayload {
    addressPath: "from" | "to";
    value: string | boolean;
}

export interface AccordionStatePayload {
    index: string;
    state: boolean;
}

export interface OptionPayload {
    index: string;
    value: string;
}

export interface DataPayload {
    key: "firstName" | "lastName" | "salutation" | "company" | "telNumber" | "email";
    value: string;
}

export enum DATA_PAYLOAD_KEYS {
    firstName = "firstName",
    lastName = "lastName",
    salutation = "salutation",
    company = "company",
    telNumber = "telNumber",
    email = "email"
}
