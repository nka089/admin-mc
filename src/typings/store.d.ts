import { Category, Item, ItemBlock, Order, Service } from "./types";

interface State {
    analytics: Analytics;
    order: Order;
    appdata: AppData;
    currentOrders: Order[];
    options: { [key: string]: string };
    categories: Category[];
    items: Item[];
    services: Service[];
    control: Control;
}

interface Analytics {
    data: any[];
    counter: number;
}

interface Control {
    showDialog: boolean;
    showHourglass: boolean;
    lastCacheKey: string;
    updateOrder: number;
}

export interface AppData {
    itemsBlocks: ItemBlock[];
}
