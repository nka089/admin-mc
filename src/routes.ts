export const routes = [
    {
        path: "auftraege",
        outlet: "auftraege",
        defaultRoute: true
    },
    {
        path: "auftrag-bearbeiten",
        outlet: "auftrag-bearbeiten"
    },
    {
        path: "leistungen",
        outlet: "leistungen"
    },
    {
        path: "moebel",
        outlet: "moebel"
    },
    {
        path: "optionen",
        outlet: "optionen"
    },
    {
        path: "kategorien",
        outlet: "kategorien"
    },
    {
        path: "analytics",
        outlet: "analytics"
    }
];
