import { registerRouterInjector } from "@dojo/framework/routing/RouterInjector";
import { Store } from "@dojo/framework/stores/Store";
import { w } from "@dojo/framework/core/vdom";
import Registry from "@dojo/framework/core/Registry";
import renderer from "@dojo/framework/core/vdom";
import "@dojo/themes/dojo/index.css";
import Application from "./application/Application";
import { routes } from "./routes";
import { State } from "./typings/store";
import { initOptionsProcess, CONTROL } from "./processes/Processes";
import { initCategories } from "./processes/CategoriesProcesses";
import { initItems } from "./processes/ItemsProcesses";
import { initServices } from "./processes/ServicesProcesses";
import theme from "@dojo/themes/dojo";
import "@dojo/themes/dojo/index.css";
import { registerThemeInjector } from "@dojo/framework/core/mixins/Themed";

const store = new Store<State>();
export const _store = (): Store<State> => {
    return store;
};

//@ts-ignore
window.logstore = () => console.log(store._adapter._state);
const registry = new Registry();

registerThemeInjector(theme, registry);

registry.defineInjector("state", () => () => store);
const _router = registerRouterInjector(routes, registry);

export const to = (toPath: string, singleParameter?: string) => {
    let path: string = toPath;
    if (singleParameter) {
        path = path.concat("/", singleParameter);
    }
    _router.setPath(path);
};

(async () => {
    CONTROL.setShowHourlassProcess(store)(true);
    await Promise.all([initServices(store), initItems(store), initCategories(store), initOptionsProcess(store)({})]);
})().then(() => {
    CONTROL.setShowHourlassProcess(store)(false);

    console.log("App Geladen");
});
const _renderer = renderer(() => w(Application, {}));
_renderer.mount({ domNode: document.getElementById("um-configurator-admin") as HTMLElement, registry });
