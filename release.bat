@echo off
SET time = %time/T%
SET date = %date/T%

echo export const version = "%time% - %date%"; > src/version.ts
SET devFolder=C:\my\dev\github\admin-mc
SET releaseFolder=C:\my\dev\gitlab\um-configurator

SET distFolder=\output\dist\*


cd /d %devFolder%
CALL .\brc.bat

cd /d %releaseFolder%
rmdir app-dist\rcAdmin /s /q
mkdir app-dist\rcAdmin


SET SRC=%devFolder%%distFolder%
SET DST=%releaseFolder%\app-dist\rcAdmin
xcopy /E %SRC% %DST%

git checkout development
git add --all
git commit -m "release: update rcAdmin"
git pull
git push
cd %devFolder%

